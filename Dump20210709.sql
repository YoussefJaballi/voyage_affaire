-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: localhost    Database: voyageaffaire
-- ------------------------------------------------------
-- Server version	5.7.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `adresse`
--

DROP TABLE IF EXISTS `adresse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `adresse` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `latitude` bigint(20) NOT NULL,
  `longitude` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `adresse`
--

LOCK TABLES `adresse` WRITE;
/*!40000 ALTER TABLE `adresse` DISABLE KEYS */;
/*!40000 ALTER TABLE `adresse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `agence`
--

DROP TABLE IF EXISTS `agence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `agence` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `entreprise_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKa12ki6rnk4iv673x7f5cmia2` (`entreprise_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agence`
--

LOCK TABLES `agence` WRITE;
/*!40000 ALTER TABLE `agence` DISABLE KEYS */;
/*!40000 ALTER TABLE `agence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `agenda`
--

DROP TABLE IF EXISTS `agenda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `agenda` (
  `id` int(11) NOT NULL,
  `date_debut` datetime DEFAULT NULL,
  `date_fin` datetime DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `agenda_utilisateur_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKcdts6hfg1ak9mu5orhyr796a` (`agenda_utilisateur_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agenda`
--

LOCK TABLES `agenda` WRITE;
/*!40000 ALTER TABLE `agenda` DISABLE KEYS */;
/*!40000 ALTER TABLE `agenda` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `avion`
--

DROP TABLE IF EXISTS `avion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `avion` (
  `id` int(11) NOT NULL,
  `capacité` int(11) NOT NULL,
  `avion_vol` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKh205a87oeira085vi8wx5d2yn` (`avion_vol`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `avion`
--

LOCK TABLES `avion` WRITE;
/*!40000 ALTER TABLE `avion` DISABLE KEYS */;
/*!40000 ALTER TABLE `avion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `billet`
--

DROP TABLE IF EXISTS `billet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `billet` (
  `id` int(11) NOT NULL,
  `date` datetime DEFAULT NULL,
  `heure` varchar(255) DEFAULT NULL,
  `nom_passport` varchar(255) DEFAULT NULL,
  `prix` double NOT NULL,
  `entreprise_id` int(11) DEFAULT NULL,
  `vol_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKjd1tl2hsxxpb66mv2uop9gukm` (`entreprise_id`),
  KEY `FKbwb707bkeoqu8ys3b7vs5tmxd` (`vol_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `billet`
--

LOCK TABLES `billet` WRITE;
/*!40000 ALTER TABLE `billet` DISABLE KEYS */;
/*!40000 ALTER TABLE `billet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chambre`
--

DROP TABLE IF EXISTS `chambre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `chambre` (
  `id` int(11) NOT NULL,
  `etage` int(11) NOT NULL,
  `numero` int(11) NOT NULL,
  `hotel_id` int(11) DEFAULT NULL,
  `chambre_reservation` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKaky3rlylfxbs2hgwjcwgjkghc` (`hotel_id`),
  KEY `FKkbj69sill0l0n4s9o0jikv9ko` (`chambre_reservation`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chambre`
--

LOCK TABLES `chambre` WRITE;
/*!40000 ALTER TABLE `chambre` DISABLE KEYS */;
/*!40000 ALTER TABLE `chambre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contrat`
--

DROP TABLE IF EXISTS `contrat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contrat` (
  `reference` int(11) NOT NULL AUTO_INCREMENT,
  `date_debut` date DEFAULT NULL,
  `salaire` float NOT NULL,
  `type_contrat` varchar(255) DEFAULT NULL,
  `employe_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`reference`),
  KEY `FKidi9k1fvw6mma24yqoe2kmtju` (`employe_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contrat`
--

LOCK TABLES `contrat` WRITE;
/*!40000 ALTER TABLE `contrat` DISABLE KEYS */;
/*!40000 ALTER TABLE `contrat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `departement`
--

DROP TABLE IF EXISTS `departement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `departement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `entreprise_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKkg0jmw8ih55tnlfet3ucbkfsy` (`entreprise_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departement`
--

LOCK TABLES `departement` WRITE;
/*!40000 ALTER TABLE `departement` DISABLE KEYS */;
INSERT INTO `departement` VALUES (1,'Informatique',1);
/*!40000 ALTER TABLE `departement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `departement_employes`
--

DROP TABLE IF EXISTS `departement_employes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `departement_employes` (
  `departement_id` int(11) NOT NULL,
  `employes_id` int(11) NOT NULL,
  KEY `FKp688tcln21xhsg34l5ltk164s` (`employes_id`),
  KEY `FK61ibwy8dqweu0jb1enlb4enlh` (`departement_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departement_employes`
--

LOCK TABLES `departement_employes` WRITE;
/*!40000 ALTER TABLE `departement_employes` DISABLE KEYS */;
/*!40000 ALTER TABLE `departement_employes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email`
--

DROP TABLE IF EXISTS `email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `email` (
  `id` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `id_envoyeur` int(11) NOT NULL,
  `id_recepteur` int(11) NOT NULL,
  `titre` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email`
--

LOCK TABLES `email` WRITE;
/*!40000 ALTER TABLE `email` DISABLE KEYS */;
INSERT INTO `email` VALUES (175,'vous souhaite à rejoindre cette plateforme pour mieux gérer les voyages',1,1,'Invitation a rejoindre le plateforme pour la gestion des agences et voyages'),(173,'neeeeewtestvoyage',1,1,'neeeeewtestvoyage'),(171,'neeeeewtestvoyage',1,1,'neeeeewtestvoyage'),(169,'neeeeewtestvoyage',1,1,'neeeeewtestvoyage'),(167,'neeeeewtestvoyage',1,1,'neeeeewtestvoyage'),(165,'neeeeewtestvoyage',1,1,'neeeeewtestvoyage'),(161,'vous souhaite à rejoindre cette plateforme pour mieux gérer les voyages',1,1,'Invitation a rejoindre le plateforme pour la gestion des agences et voyages'),(159,'vous souhaite à rejoindre cette plateforme pour mieux gérer les voyages',1,1,'Invitation a rejoindre le plateforme pour la gestion des agences et voyages'),(158,'vous souhaite à rejoindre cette plateforme pour mieux gérer les voyages',1,1,'Invitation a rejoindre le plateforme pour la gestion des agences et voyages'),(155,'vous souhaite à rejoindre cette plateforme pour mieux gérer les voyages',1,1,'Invitation a rejoindre le plateforme pour la gestion des agences et voyages'),(154,'vous souhaite à rejoindre cette plateforme pour mieux gérer les voyages',1,1,'Invitation a rejoindre le plateforme pour la gestion des agences et voyages'),(152,'neeeeewtestvoyage',1,1,'neeeeewtestvoyage'),(149,'vous souhaite à rejoindre cette plateforme pour mieux gérer les voyages',1,1,'Invitation a rejoindre le plateforme pour la gestion des agences et voyages'),(147,'neeeeewtestvoyage',1,1,'neeeeewtestvoyage'),(144,'this is neeeeewtest',1,1,'this is neeeeewtest'),(85,'testsenddescription',0,0,'another one'),(86,'testsenddescription',0,0,'another one'),(87,'testsenddescription',0,0,'another one'),(88,'testsenddescription',0,0,'another one'),(89,'testsenddescription',0,0,'another one'),(90,'testsenddescription',0,0,'another one'),(91,'testsenddescription',0,0,'another one'),(92,'testsenddescription',0,0,'another one'),(93,'testsenddescription',0,0,'another one'),(94,'testsenddescription',0,0,'another one'),(95,'testsenddescription',0,0,'another one'),(96,'testsenddescription',0,0,'another one'),(97,'testsenddescription',0,0,'another one'),(98,'testsenddescription',0,0,'another one'),(99,'testsenddescription',0,0,'another one'),(100,'testsenddescription',0,0,'another one'),(101,'testsenddescription',0,0,'another one'),(102,'testsenddescription',0,0,'another one'),(103,'testsenddescription',0,0,'another one'),(104,'testsenddescription',0,0,'another one'),(105,'testsenddescription',0,0,'another one'),(106,'testsenddescription',0,0,'another one'),(107,'testsenddescription',0,0,'another one'),(108,'testsenddescription',0,0,'another one'),(109,'testsenddescription',0,0,'another one'),(111,'testsenddescription',0,0,'another one'),(113,'testsenddescription',0,0,'another one'),(115,'testsenddescription',0,0,'another one'),(117,'testsenddescription',0,0,'another one'),(119,'testsenddescription',0,0,'another one'),(122,'testsenddescription5',0,0,'another one5'),(124,'testsenddescription5',0,0,'another one5'),(125,'testsenddescription5',0,0,'another one5'),(127,'testsenddescription5',0,0,'another one5'),(128,'testsenddescription6',1,555,'another one6'),(129,'testsenddescription6',1,1,'another one6'),(132,'test1111111',1,1,'test1111111'),(134,'neeeeewtest',1,1,'neeeeewtest'),(137,'neeeeewtestvoyage',1,1,'neeeeewtestvoyage'),(139,'neeeeewtestvoyage',1,1,'neeeeewtestvoyage'),(141,'neeeeewtest',1,1,'neeeeewtest'),(177,'vous souhaite à rejoindre cette plateforme pour mieux gérer les voyages',1,1,'Invitation a rejoindre le plateforme pour la gestion des agences et voyages'),(180,'neeeeewtestvoyage',1,1,'neeeeewtestvoyage'),(183,'vous souhaite à rejoindre cette plateforme pour mieux gérer les voyages',1,1,'Invitation a rejoindre le plateforme pour la gestion des agences et voyages'),(186,'neeeeewtestvoyage',1,185,'neeeeewtestvoyage'),(188,'vous souhaite à rejoindre cette plateforme pour mieux gérer les voyages',1,1,'Invitation a rejoindre le plateforme pour la gestion des agences et voyages'),(191,'neeeeewtestvoyage',1,190,'neeeeewtestvoyage'),(193,'neeeeewtestvoyage',1,190,'neeeeewtestvoyage'),(197,'vous souhaite à rejoindre cette plateforme pour mieux gérer les voyages',1,2,'Invitation a rejoindre le plateforme pour la gestion des agences et voyages'),(200,'neeeeewtestvoyage',1,199,'neeeeewtestvoyage'),(204,'vous souhaite à rejoindre cette plateforme pour mieux gérer les voyages',1,1,'Invitation a rejoindre le plateforme pour la gestion des agences et voyages');
/*!40000 ALTER TABLE `email` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employe`
--

DROP TABLE IF EXISTS `employe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `employe` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `numtel` int(11) NOT NULL,
  `cin` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `departements_id` int(11) DEFAULT NULL,
  `employe_adresse` int(11) DEFAULT NULL,
  `employe_chambre_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_Email` (`email`),
  UNIQUE KEY `UC_CIN` (`cin`),
  KEY `FKsfhnk7nw31ycqg88ajgr55frt` (`departements_id`),
  KEY `FKbx1u9cew84nwqhqxpnjbb2nqe` (`employe_adresse`),
  KEY `FKffyk9mfelj1b2h226w8b1n9ar` (`employe_chambre_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employe`
--

LOCK TABLES `employe` WRITE;
/*!40000 ALTER TABLE `employe` DISABLE KEYS */;
INSERT INTO `employe` VALUES (1,'test',0,'04839627',NULL,NULL,'jaballiyoussef4@gmail.com',1,NULL,NULL);
/*!40000 ALTER TABLE `employe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entreprise`
--

DROP TABLE IF EXISTS `entreprise`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `entreprise` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entreprise`
--

LOCK TABLES `entreprise` WRITE;
/*!40000 ALTER TABLE `entreprise` DISABLE KEYS */;
INSERT INTO `entreprise` VALUES (1,'vermeg'),(2,'Sopra');
/*!40000 ALTER TABLE `entreprise` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facture`
--

DROP TABLE IF EXISTS `facture`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `facture` (
  `id` int(11) NOT NULL,
  `date` datetime DEFAULT NULL,
  `numero` int(11) NOT NULL,
  `enterprisee_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK83ys8kxse0hk4n25qudn3yolh` (`enterprisee_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facture`
--

LOCK TABLES `facture` WRITE;
/*!40000 ALTER TABLE `facture` DISABLE KEYS */;
/*!40000 ALTER TABLE `facture` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (206),(206);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hotel`
--

DROP TABLE IF EXISTS `hotel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hotel` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `hotel_adresse` int(11) DEFAULT NULL,
  `hotel_voyage` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKr0ojjwskft7qwr4ktk5wqhm2i` (`hotel_adresse`),
  KEY `FK92qu1sgdunt2pmejgr6kxhfil` (`hotel_voyage`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hotel`
--

LOCK TABLES `hotel` WRITE;
/*!40000 ALTER TABLE `hotel` DISABLE KEYS */;
/*!40000 ALTER TABLE `hotel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invitation`
--

DROP TABLE IF EXISTS `invitation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `invitation` (
  `id` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `titre` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `invitation_email` int(11) DEFAULT NULL,
  `invitation_voyage` int(11) DEFAULT NULL,
  `employe_id` int(11) DEFAULT NULL,
  `enterprisee_id` int(11) DEFAULT NULL,
  `statut` bit(1) NOT NULL,
  `file` varchar(255) DEFAULT NULL,
  `id_envoyeur_id` int(11) DEFAULT NULL,
  `id_recepteur_id` int(11) DEFAULT NULL,
  `invitation_voyage_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKf6ja4oi5rr7au2o81kv1cvgfn` (`invitation_email`),
  KEY `FKqcby73vh1gxbhm2212sy4gf7t` (`employe_id`),
  KEY `FKdn0q3hqvigyv0uuon3jvldiou` (`enterprisee_id`),
  KEY `FKksutcw3p9l4lm8v5fyipfb59i` (`id_envoyeur_id`),
  KEY `FK7n5xf57b62e7cvjgxlt3x932k` (`id_recepteur_id`),
  KEY `FKfohpblk6f57sytid42axhpxj8` (`invitation_voyage`),
  KEY `FKo5d99fishqvimkllbs18hsucf` (`invitation_voyage_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invitation`
--

LOCK TABLES `invitation` WRITE;
/*!40000 ALTER TABLE `invitation` DISABLE KEYS */;
/*!40000 ALTER TABLE `invitation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pilote`
--

DROP TABLE IF EXISTS `pilote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pilote` (
  `id` int(11) NOT NULL,
  `age` varchar(255) DEFAULT NULL,
  `grade` varchar(255) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `pilote_vol` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKlmxcwdw5ok9prwphclo0dhmsj` (`pilote_vol`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pilote`
--

LOCK TABLES `pilote` WRITE;
/*!40000 ALTER TABLE `pilote` DISABLE KEYS */;
/*!40000 ALTER TABLE `pilote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservation`
--

DROP TABLE IF EXISTS `reservation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reservation` (
  `id` int(11) NOT NULL,
  `date_debut` datetime DEFAULT NULL,
  `date_fin` datetime DEFAULT NULL,
  `reservation_facture` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKb8o8w1g34as9qwavgc4orjho0` (`reservation_facture`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservation`
--

LOCK TABLES `reservation` WRITE;
/*!40000 ALTER TABLE `reservation` DISABLE KEYS */;
/*!40000 ALTER TABLE `reservation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `utilisateur` (
  `id` int(11) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `prenom` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `cin` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `related_employe` int(11) DEFAULT NULL,
  `utilisateur_entreprise_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_Email` (`email`),
  UNIQUE KEY `UC_CIN` (`cin`),
  KEY `FKb1cm2fi7lvhtppetmq42u3s7o` (`related_employe`),
  KEY `FK196b6e2ik54n7bgcqocjtcycm` (`utilisateur_entreprise_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `utilisateur`
--

LOCK TABLES `utilisateur` WRITE;
/*!40000 ALTER TABLE `utilisateur` DISABLE KEYS */;
INSERT INTO `utilisateur` VALUES (1,'youssef.jaballi@esprit.tn','jaballi','admin','youssef','youssefjbl','0489273',0,NULL,1);
/*!40000 ALTER TABLE `utilisateur` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vol`
--

DROP TABLE IF EXISTS `vol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vol` (
  `id` int(11) NOT NULL,
  `date_arrivée` datetime DEFAULT NULL,
  `date_depart` datetime DEFAULT NULL,
  `destination` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vol`
--

LOCK TABLES `vol` WRITE;
/*!40000 ALTER TABLE `vol` DISABLE KEYS */;
/*!40000 ALTER TABLE `vol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vol_billets`
--

DROP TABLE IF EXISTS `vol_billets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vol_billets` (
  `list_vol_id` int(11) NOT NULL,
  `billets_id` int(11) NOT NULL,
  `vol_id` int(11) NOT NULL,
  UNIQUE KEY `UK_owjcxvxubctjt21j923nlgwq7` (`billets_id`),
  KEY `FK3wedrdrecpvf27psmg4eykihl` (`billets_id`),
  KEY `FKe9l7ubg4ba8lemdkmdg4k4cnw` (`list_vol_id`),
  KEY `FK1pdilxn75iiab0r7e00bffj23` (`vol_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vol_billets`
--

LOCK TABLES `vol_billets` WRITE;
/*!40000 ALTER TABLE `vol_billets` DISABLE KEYS */;
/*!40000 ALTER TABLE `vol_billets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `voyage`
--

DROP TABLE IF EXISTS `voyage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `voyage` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `enterprise_id` int(11) DEFAULT NULL,
  `agenda_id` int(11) DEFAULT NULL,
  `date_debut` datetime DEFAULT NULL,
  `date_fin` datetime DEFAULT NULL,
  `voyage_utilisateur_id` int(11) DEFAULT NULL,
  `voyage_vol` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKk4nw8r7s8y6niwknsfiq9n9p` (`agenda_id`),
  KEY `FKptngjtsgb9ptxkvbfj7y91t5p` (`enterprise_id`),
  KEY `FKglw0rd1ugdd30d3utyjyy1cq2` (`voyage_vol`),
  KEY `FKc3qkutky6mxcbl7fxo8cc2w2j` (`voyage_utilisateur_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `voyage`
--

LOCK TABLES `voyage` WRITE;
/*!40000 ALTER TABLE `voyage` DISABLE KEYS */;
INSERT INTO `voyage` VALUES (21,'voyage au liban',1,NULL,'2021-03-01 11:15:45','2021-08-01 11:15:45',NULL,NULL);
/*!40000 ALTER TABLE `voyage` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-07-09 11:00:51
