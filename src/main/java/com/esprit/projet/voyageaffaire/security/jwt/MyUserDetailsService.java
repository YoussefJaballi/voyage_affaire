package com.esprit.projet.voyageaffaire.security.jwt;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.esprit.projet.voyageaffaire.entity.Utilisateur;
import com.esprit.projet.voyageaffaire.enums.UserType;
import com.esprit.projet.voyageaffaire.repository.UtilisateurRepository;

@Service
public class MyUserDetailsService implements UserDetailsService {

	@Autowired
	UtilisateurRepository utilisateurRepository;
	
	
    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
		List<Utilisateur> utilisateurs = utilisateurRepository.findAll();
		for(Utilisateur utilisateur : utilisateurs) {
			if(utilisateur.getUsername().equals(s)) {
				   storeConnectedUser(utilisateur);
			       return new User(utilisateur.getUsername(), utilisateur.getPassword(),new ArrayList<>());
			}
		}
		return null;
 
    }
    
    public UserDetails loaduserByUsernameAndpassword(String username,String password) {
    	Pageable pageable = null;
		Page<Utilisateur> utilisateur = utilisateurRepository.UserByUserNameAndPassword(username, password, pageable);
		return new User(((Utilisateur)utilisateur).getUsername(), ((Utilisateur)utilisateur).getPassword(),new ArrayList<>());
    }
    
    private void storeConnectedUser(Utilisateur utilisateur) {	
    	ConnectedUser.id = utilisateur.getId();
    	ConnectedUser.enterpriseId = utilisateur.getUtilisateur_entreprise().getId();
    	ConnectedUser.type = utilisateur.getType();
    	ConnectedUser.nom = utilisateur.getNom();
    	// need to replace this line by the correct one
    	if(UserType.Personne_physique.equals(utilisateur.getType())) {
    	ConnectedUser.related_employe_id = utilisateur.getEmploye().getId();
    	}
    	
    }
}
