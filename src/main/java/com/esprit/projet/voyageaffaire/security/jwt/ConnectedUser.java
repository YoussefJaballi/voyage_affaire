package com.esprit.projet.voyageaffaire.security.jwt;

import com.esprit.projet.voyageaffaire.enums.UserType;

public class ConnectedUser {
	
	
	public static int enterpriseId;
	public static int related_employe_id;
    public static int id;
    public static String nom;
    
    public static UserType type;

	public  int getEnterpriseId() {
		return enterpriseId;
	}

	public void setEnterpriseId(int enterpriseId) {
		this.enterpriseId = enterpriseId;
	}

	public static int getRelated_employe_id() {
		return related_employe_id;
	}

	public static void setRelated_employe_id(int related_employe_id) {
		ConnectedUser.related_employe_id = related_employe_id;
	}

	public UserType getType() {
		return type;
	}

	public void setType(UserType type) {
		this.type = type;
	}

	public static String getNom() {
		return nom;
	}

	public static void setNom(String nom) {
		ConnectedUser.nom = nom;
	}
	
	
}
