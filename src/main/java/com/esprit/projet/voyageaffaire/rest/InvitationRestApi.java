package com.esprit.projet.voyageaffaire.rest;

import java.io.IOException;
import java.util.List;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.esprit.projet.voyageaffaire.apimodel.InvitationModel;
import com.esprit.projet.voyageaffaire.apimodel.InvitationResponse;
import com.esprit.projet.voyageaffaire.service.IInvitationService;



@RestController
@RequestMapping(value = "/api/invitations")
public class InvitationRestApi {
	private String title = "Hello, I'm the Invitation Microservice";
	
	@Autowired
	private IInvitationService IInvitationService;
	
	@RequestMapping("/hello")
	public String sayHello() {
		System.out.println(title);
		return title;
	}
	@PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<InvitationResponse> updateInvitation(@PathVariable(value = "id") int id,
    										@RequestBody InvitationModel invitation){
		return new ResponseEntity<>(IInvitationService.updateInvitation(id, invitation), HttpStatus.OK);
	}
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<InvitationResponse> deleteInvitation(@PathVariable(value = "id") int id){
		return new ResponseEntity<>(IInvitationService.deleteInvitation(id), HttpStatus.OK);
	}
	
	@GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<InvitationResponse> getInvitations(){
		return new ResponseEntity<InvitationResponse>(IInvitationService.getInvitations(), HttpStatus.OK);
	}
	
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<InvitationResponse> getInvitationById(@PathVariable(value = "id") int id){
		return new ResponseEntity<InvitationResponse>(IInvitationService.getInvitationById(id), HttpStatus.OK);
	}
	
	@PostMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<InvitationResponse> createInvitation(@RequestBody InvitationModel invitation) {
		try {
			return new ResponseEntity<>(IInvitationService.addInvitation(invitation), HttpStatus.OK);
		} catch (MessagingException e) {
			e.printStackTrace();
			return new ResponseEntity<InvitationResponse>(null);
		}
	}
	
	@PostMapping(value = "/{id}/accept", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<InvitationResponse> acceptInvitation(@PathVariable(value = "id") int id,@RequestParam("file") MultipartFile file) throws IllegalStateException, IOException {
		return new ResponseEntity<>(IInvitationService.acceptInvitation(id,file), HttpStatus.OK);
	}
	
	@PostMapping(value = "/{id}/cancel", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<InvitationResponse> cancelInvitation(@PathVariable(value = "id") int id) throws IllegalStateException, IOException, MessagingException {
		return new ResponseEntity<>(IInvitationService.cancelInvitation(id), HttpStatus.OK);
	}
	
	@PostMapping(value = "/{id}/addtoaganda/{idagenda}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<InvitationResponse> addtoagenda(@PathVariable(value = "id") int id,@PathVariable(value = "idagenda") int idagenda) throws IllegalStateException, IOException {
		return new ResponseEntity<>(IInvitationService.addtoagenda(id,idagenda), HttpStatus.OK);
	}
	
	@PostMapping(value = "/{id}/resendmail", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<InvitationResponse> resendmail(@PathVariable(value = "id") int id) throws IllegalStateException, IOException, MessagingException {
		return new ResponseEntity<>(IInvitationService.resendmail(id), HttpStatus.OK);
	}
	
	@PostMapping(value = "/{id}/removeAcceptanceFile", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<InvitationResponse> removeAcceptanceFile(@PathVariable(value = "id") int id) throws IllegalStateException, IOException, MessagingException {
		return new ResponseEntity<>(IInvitationService.removeAcceptanceFile(id), HttpStatus.OK);
	}
	
	@PostMapping(value = "/{id}/addacceptancefile", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<InvitationResponse> addacceptancefile(@PathVariable(value = "id") int id,@RequestParam("file") MultipartFile file) throws IllegalStateException, IOException {
		return new ResponseEntity<>(IInvitationService.addAcceptanceFile(id,file), HttpStatus.OK);
	}
	
	
}

