package com.esprit.projet.voyageaffaire.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.esprit.projet.voyageaffaire.entity.Adresse;
import com.esprit.projet.voyageaffaire.entity.Chambre;
import com.esprit.projet.voyageaffaire.entity.Hotel;
import com.esprit.projet.voyageaffaire.errors.ApiRequestException;
import com.esprit.projet.voyageaffaire.repository.AdresseRepository;
import com.esprit.projet.voyageaffaire.repository.ChambreRepository;
import com.esprit.projet.voyageaffaire.service.impl.AdresseService;
import com.esprit.projet.voyageaffaire.service.impl.ChambreService;



@RestController
@RequestMapping(value = "/api/adresses")
public class AdresseRestApi {
	@Autowired
	private AdresseService adresseService;
	@Autowired
	private AdresseRepository adresseRepository;
	
	
		@GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
	    @ResponseStatus(HttpStatus.OK)
	    public ResponseEntity<List<Adresse>> getAllAdresses(){
			return new ResponseEntity<>(adresseService.getAllAdresses(), HttpStatus.OK);
		}
		
		@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	    @ResponseStatus(HttpStatus.OK)
	    public ResponseEntity<Adresse> getAdresseById(@PathVariable(value = "id") int AdresseId)
	    throws ApiRequestException{
			Adresse adresse =adresseRepository.findById(AdresseId).orElseThrow(()->
			new ApiRequestException("chambre non existe"+AdresseId));
			return ResponseEntity.ok().body(adresse);
		}
		
		@PostMapping
		@ResponseStatus(HttpStatus.CREATED)
		public ResponseEntity<Adresse> createAdresse(@RequestBody Adresse adresse) {
			return new ResponseEntity<>(adresseService.addAdresse(adresse), HttpStatus.OK);
		}
		@PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	    @ResponseStatus(HttpStatus.OK)
	    public ResponseEntity<Adresse> updateAdresse(@PathVariable(value = "id") int id,
	    										@RequestBody Adresse adresse){
			return new ResponseEntity<>(adresseService.updateAdresse(id, adresse), HttpStatus.OK);
		}
		@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	    @ResponseStatus(HttpStatus.OK)
	    public ResponseEntity<String> deleteAdresse(@PathVariable(value = "id") int id){
			return new ResponseEntity<>(adresseService.deleteAdresse(id), HttpStatus.OK);
		}

	
	
}
