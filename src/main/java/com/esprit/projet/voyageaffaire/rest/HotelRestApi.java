package com.esprit.projet.voyageaffaire.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.esprit.projet.voyageaffaire.entity.Adresse;
import com.esprit.projet.voyageaffaire.entity.Hotel;
import com.esprit.projet.voyageaffaire.errors.ApiRequestException;
import com.esprit.projet.voyageaffaire.repository.AdresseRepository;
import com.esprit.projet.voyageaffaire.repository.HotelRepository;
import com.esprit.projet.voyageaffaire.service.impl.AdresseService;
import com.esprit.projet.voyageaffaire.service.impl.HotelService;


@RestController
@RequestMapping(value = "/api/hotels")
public class HotelRestApi {
	
	@Autowired
	private HotelService hotelService;

	@Autowired
	private HotelRepository hotelrepository;
	
	@Autowired
	private AdresseRepository adresserepository;
	
	@Autowired
	private AdresseService adresseService;
	
	@GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<Hotel>> getAllHotels(){
		return new ResponseEntity<>(hotelService.getAllHotels(), HttpStatus.OK);
	}
	
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Hotel> getHotelById(@PathVariable(value = "id") int HotelId)
    throws ApiRequestException{
		Hotel hotel =hotelrepository.findById(HotelId).orElseThrow(()->
		new ApiRequestException("Hotel non existe"+HotelId));
		return ResponseEntity.ok().body(hotel);
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Hotel> createHotel(@RequestBody Hotel hotel) {
		return new ResponseEntity<>(hotelService.addHotel(hotel), HttpStatus.OK);
	}
	@PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Hotel> updateHotel(@PathVariable(value = "id") int id,
    										@RequestBody Hotel hotel){
		return new ResponseEntity<>(hotelService.updateHotel(id, hotel), HttpStatus.OK);
	}
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<String> deleteHotel(@PathVariable(value = "id") int id){
		return new ResponseEntity<>(hotelService.deleteHotel(id), HttpStatus.OK);
	}
	
	////http://localhost:8083/api/hotels/getAllHotelByAdresse/3
	   @GetMapping(value = "getAllHotelByAdresse/{idhotel}")
	   @ResponseBody
	   public List<String> getAllHotelByAdresse(@PathVariable("idhotel")int adrid) {
			return hotelService.getAllHotelByAdresse(adrid);
		}

	 
}
