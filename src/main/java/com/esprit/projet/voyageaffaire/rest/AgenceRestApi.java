package com.esprit.projet.voyageaffaire.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.esprit.projet.voyageaffaire.entity.Agence;
import com.esprit.projet.voyageaffaire.service.impl.AgenceService;

@RestController
@RequestMapping(value = "/api/agence")
public class AgenceRestApi {
	@Autowired
	private AgenceService agenceService;
	
	@GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<Agence>> getAllAgence(){
		return new ResponseEntity<>(agenceService.getAllAgence(), HttpStatus.OK);
	}
	
	@PostMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Agence> createEmploye(@RequestBody Agence agence) {
		return new ResponseEntity<>(agenceService.addAgence(agence), HttpStatus.OK);
	}
	
	@PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Agence> updateAgence(@PathVariable(value = "id") int id,
    										@RequestBody Agence agence){
		return new ResponseEntity<>(agenceService.updateAgence(id, agence), HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<String> deleteAgence(@PathVariable(value = "id") int id){
		return new ResponseEntity<>(agenceService.deleteAgence(id), HttpStatus.OK);
	}
}
