package com.esprit.projet.voyageaffaire.rest;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.esprit.projet.voyageaffaire.entity.Email;
import com.esprit.projet.voyageaffaire.service.IEmailService;


@RestController
@RequestMapping(value = "/api/emails")
public class EmailRestApi {
	private String title = "Hello, I'm the Email Microservice";

    @Autowired
	IEmailService IEmailService;
    
	@RequestMapping("/hello")
	public String sayHello() {
		System.out.println(title);
		return title;
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Email> createEmail(@RequestBody Email email) throws MessagingException {
		return new ResponseEntity<>(IEmailService.addEmail(email), HttpStatus.OK);
	}
	
	@PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Email> updateEmail(@PathVariable(value = "id") int id,
    										@RequestBody Email email){
		return new ResponseEntity<>(IEmailService.updateEmail(id, email), HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<String> deleteEmail(@PathVariable(value = "id") int id){
		return new ResponseEntity<>(IEmailService.deleteEmail(id), HttpStatus.OK);
	}
}

