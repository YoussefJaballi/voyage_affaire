package com.esprit.projet.voyageaffaire.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.esprit.projet.voyageaffaire.entity.Voyage;
import com.esprit.projet.voyageaffaire.service.impl.VoyageService;

@RestController
@RequestMapping(value = "/api/voyages")
public class VoyageRestApi {
	private String title = "Hello, I'm the Voyage Microservice";
	@Autowired
	private VoyageService VoyageService;
	@RequestMapping("/hello")
	public String sayHello() {
		System.out.println(title);
		return title;
	}
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Voyage> createVoyage(@RequestBody Voyage voyage) {
		return new ResponseEntity<>(VoyageService.addVoyage(voyage), HttpStatus.OK);
	}
	@PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Voyage> updateVoyage(@PathVariable(value = "id") int id,
    										@RequestBody Voyage voyage){
		return new ResponseEntity<>(VoyageService.updateVoyage(id, voyage), HttpStatus.OK);
	}
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<String> deleteVoyage(@PathVariable(value = "id") int id){
		return new ResponseEntity<>(VoyageService.deleteVoyage(id), HttpStatus.OK);
	}
	
//http://localhost:8083/api/voyages/getAllVoyageByVol/3
	   @GetMapping(value = "getAllVoyageByVol/{idVoyage}")
	   @ResponseBody
	   public List<String> getAllVoyageByVol(@PathVariable("idVoyage")int adrid) {
			return VoyageService.getAllVoyageByVol(adrid);
		}
	
}

