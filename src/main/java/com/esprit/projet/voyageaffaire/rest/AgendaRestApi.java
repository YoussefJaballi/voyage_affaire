package com.esprit.projet.voyageaffaire.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.esprit.projet.voyageaffaire.apimodel.AgendaModel;
import com.esprit.projet.voyageaffaire.apimodel.AgendaResponse;
import com.esprit.projet.voyageaffaire.entity.Agenda;
import com.esprit.projet.voyageaffaire.service.IAgendaService;


@RestController
@RequestMapping(value = "/api/agendas")
public class AgendaRestApi {
	private String title = "Hello, I'm the Agenda Microservice";

    @Autowired
    private IAgendaService IAgendaService;
	
	@RequestMapping("/hello")
	public String sayHello() {
		System.out.println(title);
		return title;
	}
	
	@PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Agenda> updateAgenda(@PathVariable(value = "id") int id,
    										@RequestBody Agenda agenda){
		return new ResponseEntity<>(IAgendaService.updateAgenda(id, agenda), HttpStatus.OK);
	}
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<String> deleteAgenda(@PathVariable(value = "id") int id){
		return new ResponseEntity<>(IAgendaService.deleteAgenda(id), HttpStatus.OK);
	}
	
	@GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<AgendaResponse> getAgendas(){
		return new ResponseEntity<AgendaResponse>(IAgendaService.getAgendas(), HttpStatus.OK);
	}
	
	@PostMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<AgendaResponse> createAgenda(@RequestBody AgendaModel agenda) {
		return new ResponseEntity<>(IAgendaService.addAgenda(agenda), HttpStatus.OK);
	}
}

