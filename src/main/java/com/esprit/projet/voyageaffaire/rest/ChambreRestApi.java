package com.esprit.projet.voyageaffaire.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.esprit.projet.voyageaffaire.entity.Chambre;
import com.esprit.projet.voyageaffaire.entity.Hotel;
import com.esprit.projet.voyageaffaire.errors.ApiRequestException;
import com.esprit.projet.voyageaffaire.repository.ChambreRepository;
import com.esprit.projet.voyageaffaire.repository.HotelRepository;
import com.esprit.projet.voyageaffaire.service.impl.ChambreService;

@RestController
@RequestMapping(value = "/api/chambres")
public class ChambreRestApi {
	
	@Autowired
	private ChambreService chambreService;
	@Autowired
	private ChambreRepository chambrerepository;


	@GetMapping(value = "/chambres", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<Chambre>> getAllChambres(@PathVariable(value = "id") int id,
    										@RequestBody Chambre chambre){
		return new ResponseEntity<>(chambreService.getAllChambres(), HttpStatus.OK);
	}
	
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Chambre> getChambreById(@PathVariable(value = "id") int ChambreId)
    throws ApiRequestException{
		Chambre chambre =chambrerepository.findById(ChambreId).orElseThrow(()->
		new ApiRequestException("chambre non existe"+ChambreId));
		return ResponseEntity.ok().body(chambre);
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Chambre> createChambre(@RequestBody Chambre chambre) {
		return new ResponseEntity<>(chambreService.addChambre(chambre), HttpStatus.OK);
	}
	@PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Chambre> updateChambre(@PathVariable(value = "id") int id,
    										@RequestBody Chambre chambre){
		return new ResponseEntity<>(chambreService.updateChambre(id, chambre), HttpStatus.OK);
	}
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<String> deleteChambre(@PathVariable(value = "id") int id){
		return new ResponseEntity<>(chambreService.deleteChambre(id), HttpStatus.OK);
	}

}
