package com.esprit.projet.voyageaffaire.rest;

import java.util.List;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.esprit.projet.voyageaffaire.apimodel.BilletModel;
import com.esprit.projet.voyageaffaire.entity.Billet;
import com.esprit.projet.voyageaffaire.entity.Vol;
import com.esprit.projet.voyageaffaire.service.impl.BilletService;
import com.esprit.projet.voyageaffaire.service.impl.VolService;

@RestController
@RequestMapping(value = "/api/billets")
public class BilletRestApi {

	@Autowired
	private BilletService billetService;

	// http://localhost:8083/api/billets/
	@GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<List<Billet>> getAllBillets() {
		return new ResponseEntity<>(billetService.getAllBillets(), HttpStatus.OK);
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Billet> createBillet(@RequestBody Billet billet) {
		return new ResponseEntity<>(billetService.addBillet(billet), HttpStatus.OK);
	}

	// http://localhost:8083/api/billets/1
	@PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<Billet> updateBillet(@PathVariable(value = "id") int id, @RequestBody Billet billet) {
		return new ResponseEntity<>(billetService.updateBillet(id, billet), HttpStatus.OK);
	}

	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<String> deleteBillet(@PathVariable(value = "id") int id) {
		return new ResponseEntity<>(billetService.deleteBillet(id), HttpStatus.OK);
	}

	//http://localhost:8083/api/billets/affecterBilletAVol/1/1
	@PutMapping(value = "/affecterBilletAVol/{idbillet}/{idvol}")
	public void affecterBilletAVol(@PathVariable("idbillet") int employeId, @PathVariable("idvol") int depId) {
		billetService.affecterBilletAVol(employeId, depId);

	}
	  @PostMapping(value = "/sendBillet", produces = MediaType.APPLICATION_JSON_VALUE)
	    @ResponseStatus(HttpStatus.OK)
	    public ResponseEntity<String> sendBillet(@RequestBody BilletModel billet) throws MessagingException{
			return new ResponseEntity<>(billetService.sendBillet(billet), HttpStatus.OK);
		}
	
}
