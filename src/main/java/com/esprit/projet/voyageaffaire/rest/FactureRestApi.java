package com.esprit.projet.voyageaffaire.rest;

import java.util.List;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.esprit.projet.voyageaffaire.apimodel.FactureModel;
import com.esprit.projet.voyageaffaire.entity.Chambre;
import com.esprit.projet.voyageaffaire.entity.Facture;
import com.esprit.projet.voyageaffaire.errors.ApiRequestException;
import com.esprit.projet.voyageaffaire.repository.ChambreRepository;
import com.esprit.projet.voyageaffaire.repository.FactureRepository;
import com.esprit.projet.voyageaffaire.service.impl.ChambreService;
import com.esprit.projet.voyageaffaire.service.impl.FactureService;


@RestController
@RequestMapping(value = "/api/factures")
public class FactureRestApi {

	@Autowired
	private FactureService factureService;
	@Autowired
	private FactureRepository facturerepository;
	@Autowired
	private ChambreService chambreService;
	

	@GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<Facture>> getAllFacturess(){
		return new ResponseEntity<>(factureService.getAllFactures(), HttpStatus.OK);
	}
	
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Facture> getFactureById(@PathVariable(value = "id") int FactureId)
    throws ApiRequestException{
		Facture facture =facturerepository.findById(FactureId).orElseThrow(()->
		new ApiRequestException("facture non existe"+FactureId));
		return ResponseEntity.ok().body(facture);
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Facture> createFacture(@RequestBody Facture facture) {
		return new ResponseEntity<>(factureService.addFacture(facture), HttpStatus.OK);
	}
	@PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Facture> updateFacture(@PathVariable(value = "id") int id,
    										@RequestBody Facture facture){
		return new ResponseEntity<>(factureService.updateFacture(id, facture), HttpStatus.OK);
	}
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<String> deleteFacture(@PathVariable(value = "id") int id){
		return new ResponseEntity<>(factureService.deleteFcture(id), HttpStatus.OK);
	}
	
///http://localhost:8083/api/factures/getAllFactureByChambre/3 avec num reservation
    @GetMapping(value = "getAllFactureByChambre/{reservationId}")
    @ResponseBody
	public List<Integer> getAllFactureByChambre(@PathVariable("reservationId") int resid) {
    	//Chambre chambre=chambreService.getChambreById(chambreId);
		return factureService.getAllFactureByChambre(resid);
	}
//    {
//    	"num_facture" : 3,
//    	"email":"amine.baklouti@esprit.tn"
//    	}
    //http://localhost:8083/api/factures/sendfacture
    
    @PostMapping(value = "/sendfacture", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<String> sendFacture(@RequestBody FactureModel facture) throws MessagingException{
		return new ResponseEntity<>(factureService.sendFacture(facture), HttpStatus.OK);
	}

}
