package com.esprit.projet.voyageaffaire.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.esprit.projet.voyageaffaire.entity.Hotel;
import com.esprit.projet.voyageaffaire.entity.Vol;
import com.esprit.projet.voyageaffaire.service.impl.HotelService;
import com.esprit.projet.voyageaffaire.service.impl.VolService;

@RestController
@RequestMapping(value = "/api/vols")
public class VolRestApi {

	@Autowired
	private VolService volService;

	@GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<List<Vol>> getAllVols() {
		return new ResponseEntity<>(volService.getAllVols(), HttpStatus.OK);
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Vol> createVol(@RequestBody Vol vol) {
		return new ResponseEntity<>(volService.addVol(vol), HttpStatus.OK);
	}

	@PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<Vol> updateVol(@PathVariable(value = "id") int id, @RequestBody Vol vol) {
		return new ResponseEntity<>(volService.updateVol(id, vol), HttpStatus.OK);
	}

	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<String> deleteVol(@PathVariable(value = "id") int id) {
		return new ResponseEntity<>(volService.deleteVol(id), HttpStatus.OK);
	}

	// http://localhost:8083/api/vols/affecterPiloteAuVol/1/2
	@PutMapping(value = "/affecterPiloteAuVol/{idpilote}/{idvol}")
	public void affecterPiloteAuVol(@PathVariable("idpilote") int piloteId, @PathVariable("idvol") int volId) {
		volService.affecterPiloteAuVol(piloteId, volId);
	}

	// http://localhost:8083/api/vols/affecterAvionAuVol/1/2
	@PutMapping(value = "/affecterAvionAuVol/{idavion}/{idvol}")
	public void affecterAvionAuVol(@PathVariable("idavion") int avionId, @PathVariable("idvol") int volId) {
		volService.affecterAvionAuVol(avionId, volId);
	}
	
	//http://localhost:8083/api/vols/affecterBilletAVol/1/1
		@PutMapping(value = "/affecterBilletAVol/{idbillet}/{idvol}")
		public void affecterBilletAVol(@PathVariable("idbillet") int employeId, @PathVariable("idvol") int depId) {
			volService.affecterBilletAVol(employeId, depId);

		}
}
