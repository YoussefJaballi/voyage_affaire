package com.esprit.projet.voyageaffaire.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.esprit.projet.voyageaffaire.entity.Adresse;
import com.esprit.projet.voyageaffaire.entity.Contrat;
import com.esprit.projet.voyageaffaire.entity.Departement;
import com.esprit.projet.voyageaffaire.entity.Employe;
import com.esprit.projet.voyageaffaire.entity.Entreprise;
import com.esprit.projet.voyageaffaire.entity.Hotel;
import com.esprit.projet.voyageaffaire.errors.ApiRequestException;
import com.esprit.projet.voyageaffaire.repository.DepartementRepository;
import com.esprit.projet.voyageaffaire.repository.EmployeRepository;
import com.esprit.projet.voyageaffaire.service.IEmployeService;
import com.esprit.projet.voyageaffaire.service.IEntrepriseService;
import com.esprit.projet.voyageaffaire.service.impl.AdresseService;
import com.esprit.projet.voyageaffaire.service.impl.DepartementService;
import com.esprit.projet.voyageaffaire.service.impl.EmployeService;
import com.esprit.projet.voyageaffaire.service.impl.HotelService;





@RestController
@RequestMapping(value = "/api/employes")
public class EmployeRestApi {
	private String title = "Hello, I'm the Employe Microservice";
	@Autowired
	private EmployeService employeService;
	@Autowired
	private EmployeRepository employerep;
	@Autowired
	 IEmployeService iemployeservice;
	@Autowired
	private HotelService hotelService;
	@Autowired
	DepartementService deptservice;
	@Autowired
	AdresseService adresseservice;
	
	   
//	@Qualifier("ientrepriseservice")
	@Autowired
	private IEntrepriseService ientrepriseservice;

	
	@RequestMapping("/hello")
	public String sayHello() {
		System.out.println(title);
		return title;
	}
	@GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<Employe>> getAllEmployess(){
		return new ResponseEntity<>(employeService.getAllEmployess(), HttpStatus.OK);
	}
	
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Employe> getEmployeById(@PathVariable(value = "id") int employeId)
    throws ApiRequestException{
		Employe employe =employerep.findById(employeId).orElseThrow(()->
		new ApiRequestException("employe non existe"+employeId));
		return ResponseEntity.ok().body(employe);
	}
	
	
	@PostMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Employe> createEmploye(@RequestBody Employe employe) {
		return new ResponseEntity<>(employeService.addEmploye(employe), HttpStatus.OK);
	}
	@PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Employe> updateEmploye(@PathVariable(value = "id") int id,
    										@RequestBody Employe employe){
		return new ResponseEntity<>(employeService.updateEmploye(id, employe), HttpStatus.OK);
	}
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<String> deleteEmploye(@PathVariable(value = "id") int id){
		return new ResponseEntity<>(employeService.deleteEmploye(id), HttpStatus.OK);
	}
	
	 // URL : http://localhost:8083/api/employes/getSalaireMoyenByDepartementId/1
    @GetMapping(value = "getSalaireMoyenByDepartementId/{iddept}")
    @ResponseBody
	public Double getSalaireMoyenByDepartementId(@PathVariable("iddept")int departementId) {
		return iemployeservice.getSalaireMoyenByDepartementId(departementId);
	}
    
 // Modifier email : http://localhost:8083/api/employes/modifyEmail/1/newemail
 	@PutMapping(value = "/modifyEmail/{id}/{newemail}") 
 	@ResponseBody
 	public void mettreAjourEmailByEmployeId(@PathVariable("newemail") String email, @PathVariable("id") int employeId) {
 		iemployeservice.mettreAjourEmailByEmployeId(email, employeId);
}
 	
 	
 // http:http://localhost:8083/api/employes/affecterEmployeADepartement/1/1
 	@PutMapping(value = "/affecterEmployeADepartement/{idemp}/{iddept}") 
 	public void affecterEmployeADepartement(@PathVariable("idemp")int employeId, @PathVariable("iddept")int depId) {
 		iemployeservice.affecterEmployeADepartement(employeId, depId);
 		
 	}
 	
 	//http://localhost:8083/api/employes/desaffecterEmployeDuDepartement/1/1
 	@PutMapping(value = "/desaffecterEmployeDuDepartement/{idemp}/{iddept}") 
 	public void desaffecterEmployeDuDepartement(@PathVariable("idemp")int employeId, @PathVariable("iddept")int depId)
 	{
 		iemployeservice.desaffecterEmployeDuDepartement(employeId, depId);
 	}

 	// http://localhost:8083/api/employes/ajouterContrat
 	//{"reference":6,"dateDebut":"2020-03-01","salaire":2000,"typeContrat":"CDD"}
 	@PostMapping("/ajouterContrat")
 	@ResponseBody
 	public int ajouterContrat(@RequestBody Contrat contrat) {
 		iemployeservice.ajouterContrat(contrat);
 		return contrat.getReference();
 	}
 	
 	// http://localhost:8083/api/employes/affecterContratAEmploye/1/1
    @PutMapping(value = "/affecterContratAEmploye/{idcontrat}/{idemp}") 
 	public void affecterContratAEmploye(@PathVariable("idcontrat")int contratId, @PathVariable("idemp")int employeId)
 	{
 		iemployeservice.affecterContratAEmploye(contratId, employeId);
 	}

 // URL : http://localhost:8083/api/employes/deleteContratById/2
    @DeleteMapping("/deleteContratById/{idcontrat}") 
	@ResponseBody
	public void deleteContratById(@PathVariable("idcontrat")int contratId) {
		iemployeservice.deleteContratById(contratId);
	}
    
 // URL : http://localhost:8083/api/employes/getNombreEmployeJPQL
    @GetMapping(value = "getNombreEmployeJPQL")
    @ResponseBody
	public int getNombreEmployeJPQL() {
		
		return iemployeservice.getNombreEmployeJPQL();
	}
    
 // URL : http://localhost:8083/api/employes/getNombreEmployebyDepartement
    @GetMapping(value = "getNombreEmployebyDepartement/{iddepartment}")
    @ResponseBody
	public int getNombreEmployeDepartement(@PathVariable("iddepartment") int iddepartment) {
    	Departement dep=deptservice.getDepartementById(iddepartment);
		return employeService.getNombreEmployeDepartement(dep);
	}
    // URL : http://localhost:8083/api/employes/getAllEmployeNamesJPQL
    @GetMapping(value = "getAllEmployeNamesJPQL")
    @ResponseBody
	public List<String> getAllEmployeNamesJPQL() {
		
		return iemployeservice.getAllEmployeNamesJPQL();
	}
    
    // URL : http://localhost:8083/api/employes/getAllEmployeByEntreprise/1
    @GetMapping(value = "getAllEmployeByEntreprise/{identreprise}")
    @ResponseBody
	public List<Employe> getAllEmployeByEntreprise(@PathVariable("identreprise") int identreprise) {
    	Entreprise entreprise=ientrepriseservice.getEntrepriseById(identreprise);
		return iemployeservice.getAllEmployeByEntreprise(entreprise);
	}

	///////http://localhost:8083/api/employes/getAllEmployeByAdresse/2
	  @GetMapping(value = "getAllEmployeByAdresse/{idemp}")
	   @ResponseBody
	   public List<String> getAllEmployeByAdresse(@PathVariable("idemp")int adrid) {
			return employeService.getAllEmployeByAdresse(adrid);
		}
	  
	  @GetMapping(value = "getAllEmployeByAdress/{idadresse}")
	    @ResponseBody
		public List<Employe> getAllEmployeByAdress(@PathVariable("idadresse") int idadresse) {
	    	Adresse adresse=adresseservice.getAdresseById(idadresse);
			return employeService.getAllEmployeByAdress(adresse);
		}
	  
	 //http://localhost:8083/api/employes/getAllEmployeByHotel/1
	  @GetMapping(value = "getAllEmployeByHotel/{idhotel}")
	    @ResponseBody
		public List<Employe> getAllEmployeByHotel(@PathVariable("idhotel") int idhotel) {
	    	Hotel hotel=hotelService.getHotelById(idhotel);
			return employeService.getAllEmployeByHotel(hotel);
		}
	  
	 //http://localhost:8083/api/employes/affecterEmployeAChambre/4/1/1
	  @PutMapping(value = "/affecterEmployeAChambre/{idemp}/{chaId}/{iddepartment}") 
		public void affecterEmployeAChambre(@PathVariable("idemp")int employeId, @PathVariable("chaId")int chaId,
				@PathVariable("iddepartment") int iddepartment) {
		  Departement dep=deptservice.getDepartementById(iddepartment);
		  employeService.affecterEmployeAChambre(employeId, chaId, dep);
			
		}
	  
	 // http://localhost:8083/api/employes/getAllEmployeByDepartement/1
	  @GetMapping(value = "getAllEmployeByDepartement/{iddepartment}")
	    @ResponseBody
		public List<Employe> getAllEmployeByDepartement(@PathVariable("iddepartment") int iddepartment) {
	    	Departement dep=deptservice.getDepartementById(iddepartment);
			return employeService.getAllEmployeByDepartement(dep);
		}
}

