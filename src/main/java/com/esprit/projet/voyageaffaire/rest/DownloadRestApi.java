package com.esprit.projet.voyageaffaire.rest;
 
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.esprit.projet.voyageaffaire.service.impl.FileService;

 
@Controller
public class DownloadRestApi {
 
	 private static final String DIRECTORY = "C:/PDF";
	    private static final String DEFAULT_FILE_NAME = "java-tutorial.pdf";
	 
	    @Autowired
	    private ServletContext servletContext;
	    
	    @Autowired
		private FileService fileService;
	 
	    // http://localhost:8083/download1?fileName=abc.zip
	    // Using ResponseEntity<InputStreamResource>
	    @RequestMapping("/download1")
	    public ResponseEntity<InputStreamResource> downloadFile1(
	            @RequestParam(defaultValue = DEFAULT_FILE_NAME) String fileName) throws IOException {
	 
	        MediaType mediaType = fileService.getMediaTypeForFileName(this.servletContext, fileName);
	        System.out.println("fileName: " + fileName);
	        System.out.println("mediaType: " + mediaType);
	 
	        File file = new File(DIRECTORY + "/" + fileName);
	        InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
	 
	        return ResponseEntity.ok()
	                // Content-Disposition
	                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + file.getName())
	                // Content-Type
	                .contentType(mediaType)
	                // Contet-Length
	                .contentLength(file.length()) //
	                .body(resource);
	    }
}