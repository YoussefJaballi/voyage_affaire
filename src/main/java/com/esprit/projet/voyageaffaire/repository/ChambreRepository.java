package com.esprit.projet.voyageaffaire.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;


import com.esprit.projet.voyageaffaire.entity.Chambre;



public interface ChambreRepository extends JpaRepository<Chambre, Integer>{


	
}
