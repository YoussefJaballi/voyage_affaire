package com.esprit.projet.voyageaffaire.repository;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.esprit.projet.voyageaffaire.entity.Adresse;
import com.esprit.projet.voyageaffaire.entity.Departement;
import com.esprit.projet.voyageaffaire.entity.Email;
import com.esprit.projet.voyageaffaire.entity.Employe;
import com.esprit.projet.voyageaffaire.entity.Entreprise;
import com.esprit.projet.voyageaffaire.entity.Hotel;




public interface EmployeRepository extends JpaRepository<Employe, Integer> {
	
		
	@Modifying
    @Transactional
    @Query("UPDATE Employe e SET e.email=:email1 where e.id=:employeId")
    public void mettreAjourEmailByEmployeIdJPQL(@Param("email1")String email, @Param("employeId")int employeId);

	
	 @Query("Select "
				+ "DISTINCT AVG(cont.salaire) from Contrat cont "
				+ "join cont.employe emp "
				+ "join emp.departements deps "
				+ "where deps.id=:depId")
	    public Double getSalaireMoyenByDepartementId(@Param("depId")int departementId);
	 
	 
	 @Query("SELECT count(*) FROM Employe")
	    public int countemp();
		
	    @Query("SELECT nom FROM Employe")
	    public List<String> employeNames();
	    
	    @Query("Select "
				+ "DISTINCT emp from Employe emp "
				+ "join emp.departements dps "
				+ "join dps.entreprise entrep "
				+ "where entrep=:entreprise")
	    public List<Employe> getAllEmployeByEntreprisec(@Param("entreprise") Entreprise entreprise);
	    
	    @Modifying
	    @Transactional
	    @Query("DELETE from Contrat")
	    public void deleteAllContratJPQL();
	    
	    @Query("SELECT count(*) FROM Employe a join a.departements h where h=:departements")
	    public int countEmpByDepartement(@Param("departements")Departement departements);
	    //
	    @Query("select a from Employe a join a.departements h where h=:departements")
	    public List<Employe> getAllEmployeByDepartement(@Param("departements")Departement departements);
//	    
//	    @Query("select c.salaire from Contrat c join c.employe e where e.id=:employeId")
//	    public float getSalaireByEmployeIdJPQL(@Param("employeId")int employeId);
	    
	    @Query("select a.nom from Employe a join a.adresse h where a.id=:adrid")
	    public List<String> getAllEmployeByAdresse(@Param("adrid")int adrid);
	    @Query("select a from Employe a join a.adresse h where h=:adresse")
	    public List<Employe> getAllEmployeByAdress(@Param("adresse")Adresse adresse);
	    
	   @Query("Select "
				+ "DISTINCT emp from Employe emp "
				+ "join emp.adresse dps "
				+ "join dps.hotel entrep "
				+ "where entrep=:hotel")
	    public List<Employe> getAllEmployeByHotel(@Param("hotel") Hotel hotel);
}
