package com.esprit.projet.voyageaffaire.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.esprit.projet.voyageaffaire.entity.Billet;

public interface BilletRepository extends JpaRepository<Billet, Integer>{

}
