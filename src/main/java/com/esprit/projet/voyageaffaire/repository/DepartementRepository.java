package com.esprit.projet.voyageaffaire.repository;

import org.springframework.data.repository.CrudRepository;

import com.esprit.projet.voyageaffaire.entity.Departement;

public interface DepartementRepository extends CrudRepository<Departement, Integer> {

}
