package com.esprit.projet.voyageaffaire.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.esprit.projet.voyageaffaire.entity.Contrat;

@Repository
public interface ContratRepository extends CrudRepository<Contrat, Integer>{

}
