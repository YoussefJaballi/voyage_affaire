package com.esprit.projet.voyageaffaire.repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.esprit.projet.voyageaffaire.entity.Invitation;



public interface InvitationRepository extends JpaRepository<Invitation, Integer> {
	
	/*@Query("select i from Invitation i where i.invitation_voyage like :voyageId")
	public Invitation invitationByVoyage(@Param("voyageId") int i);*/
	
}
