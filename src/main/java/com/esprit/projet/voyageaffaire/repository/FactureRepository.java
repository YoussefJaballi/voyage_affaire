package com.esprit.projet.voyageaffaire.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.esprit.projet.voyageaffaire.entity.Chambre;
import com.esprit.projet.voyageaffaire.entity.Entreprise;
import com.esprit.projet.voyageaffaire.entity.Facture;





public interface FactureRepository extends JpaRepository<Facture, Integer>{
	


	
//	@Query("Select "
//			+ "emp.numero from Facture emp "
//			+ "join emp.reservation dps "
//			+ "join dps.chambre entrep "
//			+ "where emp.id=:resid")
//    public  List<Integer> getAllFactureByChambre(@Param("resid") int resid);
	
	
	@Query("Select "
			+ "emp.numero from Facture emp "
			+ "join emp.reservation dps "
			+ "join dps.chambre entrep "
			+ "where emp.id=:chaid")
    public  List<Integer> getAllFactureByChambre(@Param("chaid") int chaid);
	

}
