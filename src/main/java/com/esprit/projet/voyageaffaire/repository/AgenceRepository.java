package com.esprit.projet.voyageaffaire.repository;

import org.springframework.data.repository.CrudRepository;

import com.esprit.projet.voyageaffaire.entity.Agence;

public interface AgenceRepository extends CrudRepository<Agence, Integer>{

}
