package com.esprit.projet.voyageaffaire.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.esprit.projet.voyageaffaire.entity.Avion;

public interface AvionRepository extends JpaRepository<Avion, Integer>{

}
