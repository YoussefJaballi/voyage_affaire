package com.esprit.projet.voyageaffaire.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.esprit.projet.voyageaffaire.entity.Voyage;

public interface VoyageRepository extends JpaRepository<Voyage, Integer> {
	@Query("select c from Voyage c where c.nom like :nom")
	public Page<Voyage> VoyageByNom(@Param("nom") String n, Pageable pageable);
	
	 @Query("select a.nom from Voyage a join a.vol h where a.id=:adrid")
	    public List<String> getAllVoyageByVol(@Param("adrid")int adrid);
	   
}
