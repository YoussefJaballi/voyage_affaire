package com.esprit.projet.voyageaffaire.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.esprit.projet.voyageaffaire.entity.Vol;

public interface VolRepository extends JpaRepository<Vol, Integer> {

}
