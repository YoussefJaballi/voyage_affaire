package com.esprit.projet.voyageaffaire.repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.esprit.projet.voyageaffaire.entity.Entreprise;

public interface EnterpriseRepository extends JpaRepository<Entreprise, Integer> {
	
}
