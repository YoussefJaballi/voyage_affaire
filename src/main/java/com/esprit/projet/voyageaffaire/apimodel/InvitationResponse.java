package com.esprit.projet.voyageaffaire.apimodel;

import java.util.List;

public class InvitationResponse {
	
	
	private List<InvitationModel> data;
	
	private String report;
	
	public List<InvitationModel> getData() {
		return data;
	}

	public void setData(List<InvitationModel> data) {
		this.data = data;
	}

	public String getReport() {
		return report;
	}

	public void setReport(String report) {
		this.report = report;
	}


}
