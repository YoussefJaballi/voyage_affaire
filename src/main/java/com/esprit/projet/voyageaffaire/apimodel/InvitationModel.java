package com.esprit.projet.voyageaffaire.apimodel;
import com.esprit.projet.voyageaffaire.enums.InvitationType;

public class InvitationModel {
	
   private int id;
	
	private String titre,description,type,fichier_acceptation;
	private EmailModel emailModel;
	private VoyageModel voyageModel;
	private EmployeModel employe;
	private EnterpriseModel enterprise;
	private boolean statut;
	
	

	public EnterpriseModel getEnterprise() {
		return enterprise;
	}

	public void setEnterprise(EnterpriseModel enterprise) {
		this.enterprise = enterprise;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public EmailModel getEmailModel() {
		return emailModel;
	}

	public void setEmailModel(EmailModel email) {
		this.emailModel = email;
	}

	public VoyageModel getVoyageModel() {
		return voyageModel;
	}

	public void setVoyageModel(VoyageModel voyage) {
		this.voyageModel = voyage;
	}

	
	public EmployeModel getEmploye() {
		return employe;
	}

	public void setEmploye(EmployeModel employe) {
		this.employe = employe;
	}

	public boolean isStatut() {
		return statut;
	}

	public void setStatut(boolean statut) {
		this.statut = statut;
	}

	public String getFichier_acceptation() {
		return fichier_acceptation;
	}

	public void setFichier_acceptation(String fichier_acceptation) {
		this.fichier_acceptation = fichier_acceptation;
	}
	
	
	
}
