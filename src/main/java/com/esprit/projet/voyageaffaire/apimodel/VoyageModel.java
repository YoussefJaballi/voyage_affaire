package com.esprit.projet.voyageaffaire.apimodel;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.esprit.projet.voyageaffaire.entity.Entreprise;

public class VoyageModel implements Serializable{
	
	private int id;
	private String nom;
	private String statut_invitation;
	private Date dateDebut,dateFin;
	
	public int getId() {
		return id;
	}
	public String getStatut_invitation() {
		return statut_invitation;
	}
	
	public void setStatut_invitation(String statut_invitation) {
		this.statut_invitation = statut_invitation;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public Date getDateDebut() {
		return dateDebut;
	}
	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}
	public Date getDateFin() {
		return dateFin;
	}
	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}
	
}

