package com.esprit.projet.voyageaffaire.apimodel;

import java.util.List;

public class AgendaResponse {
	
	
	private List<AgendaModel> data;
	
	private String report;
	
	public List<AgendaModel> getData() {
		return data;
	}

	public void setData(List<AgendaModel> data) {
		this.data = data;
	}

	public String getReport() {
		return report;
	}

	public void setReport(String report) {
		this.report = report;
	}


}
