package com.esprit.projet.voyageaffaire.apimodel;

import java.util.Date;
import java.util.List;

import com.esprit.projet.voyageaffaire.entity.Voyage;

public class AgendaModel {
	
private int id;
	
	private String nom,creer_par;
	private Date dateDebut,dateFin;
	private List<VoyageModel> voyages;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Date getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}

	public Date getDateFin() {
		return dateFin;
	}

	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}

	public List<VoyageModel> getVoyages() {
		return voyages;
	}

	public void setVoyages(List<VoyageModel> voyages) {
		this.voyages = voyages;
	}

	public String getCreer_par() {
		return creer_par;
	}

	public void setCreer_par(String creer_par) {
		this.creer_par = creer_par;
	}


}
