package com.esprit.projet.voyageaffaire.apimodel;

import com.esprit.projet.voyageaffaire.entity.Utilisateur;

public class EmailModel {
	
	
    private int id;
	
	private String envoyer_par,recu_par;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEnvoyer_par() {
		return envoyer_par;
	}

	public void setEnvoyer_par(String envoyer_par) {
		this.envoyer_par = envoyer_par;
	}

	public String getRecu_par() {
		return recu_par;
	}

	public void setRecu_par(String recu_par) {
		this.recu_par = recu_par;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	private String titre,description;

}
