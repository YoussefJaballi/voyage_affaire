package com.esprit.projet.voyageaffaire.apimodel;

public class FactureModel {
	private int num_facture;
	private String email;
	
	public int getNum_facture() {
		return num_facture;
	}
	public void setNum_facture(int num_facture) {
		this.num_facture = num_facture;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	

}
