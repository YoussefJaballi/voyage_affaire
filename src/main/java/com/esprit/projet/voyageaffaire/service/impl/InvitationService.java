package com.esprit.projet.voyageaffaire.service.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.web.multipart.MultipartFile;
import com.esprit.projet.voyageaffaire.apimodel.EmailModel;
import com.esprit.projet.voyageaffaire.apimodel.EmployeModel;
import com.esprit.projet.voyageaffaire.apimodel.EnterpriseModel;
import com.esprit.projet.voyageaffaire.apimodel.InvitationModel;
import com.esprit.projet.voyageaffaire.apimodel.InvitationResponse;
import com.esprit.projet.voyageaffaire.apimodel.VoyageModel;
import com.esprit.projet.voyageaffaire.entity.Agenda;
import com.esprit.projet.voyageaffaire.entity.Email;
import com.esprit.projet.voyageaffaire.entity.Employe;
import com.esprit.projet.voyageaffaire.entity.Entreprise;
import com.esprit.projet.voyageaffaire.entity.Invitation;
import com.esprit.projet.voyageaffaire.entity.Utilisateur;
import com.esprit.projet.voyageaffaire.entity.Voyage;
import com.esprit.projet.voyageaffaire.enums.ActionType;
import com.esprit.projet.voyageaffaire.enums.InvitationType;
import com.esprit.projet.voyageaffaire.enums.UserType;
import com.esprit.projet.voyageaffaire.errors.ApiRequestException;
import com.esprit.projet.voyageaffaire.repository.AgendaRepository;
import com.esprit.projet.voyageaffaire.repository.EmailRepository;
import com.esprit.projet.voyageaffaire.repository.EmployeRepository;
import com.esprit.projet.voyageaffaire.repository.EnterpriseRepository;
import com.esprit.projet.voyageaffaire.repository.InvitationRepository;
import com.esprit.projet.voyageaffaire.repository.UtilisateurRepository;
import com.esprit.projet.voyageaffaire.repository.VoyageRepository;
import com.esprit.projet.voyageaffaire.security.jwt.ConnectedUser;
import com.esprit.projet.voyageaffaire.service.IInvitationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.mail.MessagingException;
import java.io.IOException;

@Service
public class InvitationService implements IInvitationService{

	@Autowired
	private InvitationRepository invitationRepository;
	
	@Autowired
	private EmailRepository emailRepository;
	
	@Autowired
	private VoyageRepository voyageRepository;
	
	
	@Autowired
	private EmployeRepository employeRepository;
	
	@Autowired
	private UtilisateurRepository utilisateurRepository;
	
	@Autowired
	private EnterpriseRepository enterpriseRepository;
	
	@Autowired
	private AgendaRepository agendarepository;
	
	@Autowired
	private EmailService emailService;
	
	@Autowired
	private FileService fileService;
	
	

	@Override
	public InvitationResponse addInvitation(InvitationModel invitationModel) throws MessagingException {
		  InvitationResponse invitationResponse = new InvitationResponse();
		  Invitation invitation = new Invitation();
		  Employe existingEmploye = new Employe();
		  Entreprise enterprise = enterpriseRepository.findById(ConnectedUser.enterpriseId).get();
		  validateInvitationModel(invitationModel);
		  invitation.setDescription(invitationModel.getDescription());
		  invitation.setTitre(invitationModel.getTitre());
		  invitation.setType(InvitationType.valueOf(invitationModel.getType()));
		  invitation.setEnterprise(enterprise);
		  invitation.setId_envoyeur(utilisateurRepository.findById(ConnectedUser.id).get());
		  Email email = new Email();
		  email.setDescription(invitationModel.getEmailModel().getDescription());
		  email.setTitre(invitationModel.getEmailModel().getTitre());
		  email.setIdEnvoyeur(ConnectedUser.id);
		  existingEmploye = employeRepository.findById(invitationModel.getEmploye().getId()).get();
		  invitation.setEmploye(existingEmploye);
		  if (existingEmploye.getDepartements().getEntreprise().getId() == ConnectedUser.enterpriseId && UserType.Personne_morale.equals(ConnectedUser.type)) {
		    if (InvitationType.RejoindrePlateform.equals(InvitationType.valueOf(invitationModel.getType()))) {
		      emailService.sendEmail(existingEmploye.getEmail(), enterprise.getNom() + " : " + invitationModel.getEmailModel().getDescription(), invitationModel.getEmailModel().getTitre(), null);
		      email.setIdRecepteur(invitationModel.getEmploye().getId());
		    } else if (InvitationType.Voyage.equals(InvitationType.valueOf(invitationModel.getType()))) {
		      Utilisateur utilisateur = utilisateurRepository.UserByCin(existingEmploye.getCin());
		      int voyageId = invitationModel.getVoyageModel().getId();
		      Voyage voyage = voyageRepository.findById(voyageId).get();
		      invitation.setInvitation_voyage(voyage);
		      emailService.sendEmail(existingEmploye.getEmail(), enterprise.getNom() + " : " + invitationModel.getEmailModel().getDescription(), invitationModel.getEmailModel().getTitre(), fileService.getFile_path());
		      email.setIdRecepteur(utilisateur.getId());
		      invitation.setId_recepteur(utilisateur);
		    }
		    Email savedEmail = emailRepository.save(email);
		    invitation.setEmail(savedEmail);
		    invitationRepository.save(invitation);
		  }
		  invitationResponse.setReport("invitation envoyé avec succès");
		  return invitationResponse;
		}
	
	@Override
	public InvitationResponse updateInvitation(int id, InvitationModel newInvitation) {
		//modifiy only invitation related to user
		InvitationResponse invitationResponse = new InvitationResponse();
	    Invitation existingInvitation = validateInvitationById(id,ActionType.Modification);
		existingInvitation.setTitre(newInvitation.getTitre());
		existingInvitation.setDescription(newInvitation.getDescription());
		existingInvitation.setType(InvitationType.valueOf(newInvitation.getType()));
		invitationRepository.save(existingInvitation);
			/*
			 * to be completed
			 * if(newInvitation.getType().equals(InvitationType.RejoindrePlateform) && ) {
				
			}*/	
		invitationResponse.setReport("modifier avec succès");
		return invitationResponse;
		 
	}
	
	@Override
	public InvitationResponse deleteInvitation(int id) {
		InvitationResponse invitationResponse = new InvitationResponse();
		Invitation existingInvitation = validateInvitationById(id,ActionType.Suppression);
		if(existingInvitation != null) {
		invitationRepository.deleteById(id);
		invitationResponse.setReport("supprimer avec succès");
		}
		return invitationResponse;
		
	}
	
	@Override
	public InvitationResponse getInvitations() {
		List<Invitation> invitations = invitationRepository.findAll();
		List<InvitationModel> invationResponses = new ArrayList<InvitationModel>();
		InvitationResponse invitationResponse =  new InvitationResponse();
		
		if(invitations != null && !invitations.isEmpty()) {
			for(Invitation invitation  : invitations) {
				if((UserType.Personne_morale.equals(ConnectedUser.type)&& invitation.getEnterprise().getId() == ConnectedUser.enterpriseId) || 
				   (UserType.Personne_physique.equals(ConnectedUser.type) && invitation.getId_recepteur() != null && invitation.getId_recepteur().getId() == ConnectedUser.id)
						) {
				InvitationModel invitaiontModel = buildInvitationModelFromInvitation(invitation);
				
				/*
				 * to be completed
				emailModel.setRecu_par(recu_par);
				*/
				invationResponses.add(invitaiontModel);
				}
			}
			
		}
		
		invitationResponse.setData(invationResponses);
		invitationResponse.setReport("liste récupérer avec succès");
		return invitationResponse;

	}
	
	@Override
	public InvitationResponse getInvitationById(int id) {
		  List < InvitationModel > invationResponses = new ArrayList < InvitationModel > ();
		  InvitationResponse invitationResponse = new InvitationResponse();
		  Invitation existingInvitation = new Invitation();
		    existingInvitation = validateInvitationById(id,ActionType.Consultation);
		      InvitationModel invitaiontModel = buildInvitationModelFromInvitation(existingInvitation);

		      /*
		       * to be completed
		      emailModel.setRecu_par(recu_par);
		      */
		      invationResponses.add(invitaiontModel);
		    
		  invitationResponse.setData(invationResponses);
		  invitationResponse.setReport("invitation récupérer avec succès");
		  return invitationResponse;

		}
	
	@Override
	public InvitationResponse acceptInvitation(int id,MultipartFile file) throws IllegalStateException, IOException {
		//add check to verifiy if invitation date is not the same as voyage date
		// add check if status already accepted
		InvitationResponse invitationResponse = new InvitationResponse();
		Utilisateur utilisateur = new Utilisateur();
			Invitation existingInvitation = validateInvitationById(id,ActionType.Accept);
			if(InvitationType.RejoindrePlateform.equals(existingInvitation.getType()) && existingInvitation.getId_envoyeur().getId() == ConnectedUser.id && !existingInvitation.isStatut()) {
			existingInvitation.setStatut(true);
			invitationRepository.save(existingInvitation);
			utilisateur.setEmail(existingInvitation.getEmploye().getEmail());
			utilisateur.setNom(existingInvitation.getEmploye().getNom());
			utilisateur.setUsername(existingInvitation.getEmploye().getNom());
			utilisateur.setCin(existingInvitation.getEmploye().getCin());
			utilisateur.setPassword(existingInvitation.getEmploye().getCin());
			utilisateur.setType(UserType.Personne_physique);
			utilisateur.setUtilisateur_entreprise(existingInvitation.getEnterprise());
			utilisateur.setEmploye(employeRepository.findById(existingInvitation.getEmploye().getId()).get());
			utilisateurRepository.save(utilisateur);
			}
			else if(InvitationType.Voyage.equals(existingInvitation.getType()) && existingInvitation.getId_recepteur().getId() == ConnectedUser.id && !existingInvitation.isStatut()) {
				existingInvitation.setStatut(true);
				existingInvitation.setFile(fileService.uploadFile(file));
				utilisateur = utilisateurRepository.findById(ConnectedUser.id).get();
				Voyage existingVoyage = existingInvitation.getInvitation_voyage();
				existingVoyage.setVoyage_utilisateur(utilisateur);
				List<Voyage> voyages = utilisateur.getVoyages();
				voyages.add(existingVoyage);
				utilisateur.setVoyages(voyages);
				invitationRepository.save(existingInvitation);
				voyageRepository.save(existingVoyage);
				utilisateurRepository.save(utilisateur);
			}
			invitationResponse.setReport("invitation accepté");
			return invitationResponse;
		
	}
	
	@Override
	public InvitationResponse addtoagenda(int id, int idagenda) {
	    InvitationResponse invitationResponse = new InvitationResponse();
	    if (invitationRepository.findById(id).isPresent()) {
	        Invitation existingInvitation = invitationRepository.findById(id).get();
	        validateInvitationAgenda(existingInvitation);
	        if (agendarepository.findById(idagenda).isPresent()) {
	            Voyage existingvoyage = voyageRepository.findById(existingInvitation.getInvitation_voyage().getId()).get();
	            Agenda existingagenda = agendarepository.findById(idagenda).get();
	            if (existingvoyage.getAgenda() != null && existingvoyage.getAgenda().getId() == idagenda) {
	                throw new ApiRequestException("Vous avez deja ajouter cette invitation sur cette agenda");
	            } else if (existingvoyage.getDateDebut().after(existingagenda.getDateFin()) || existingvoyage.getDateFin().before(existingagenda.getDateDebut())) {
	                throw new ApiRequestException("Vous ne pouvez placer cette invitation sur cette agenda");
	            }
	            existingvoyage.setAgenda(existingagenda);
	            voyageRepository.save(existingvoyage);
	        } else {
	            throw new ApiRequestException("Agenda non existe");
	        }
	    } else {
	        throw new ApiRequestException("Invitation non existe");
	    }
	    invitationResponse.setReport("invitation affecté dans agenda");
	    return invitationResponse;
	}
	
	
	//la consultation de la liste des emails,envoie de mail a partir de web service email
	@Override
	public InvitationResponse resendmail(int id) throws MessagingException{
		
		InvitationResponse invitationResponse = new InvitationResponse();
		if (invitationRepository.findById(id).isPresent()) {
			Invitation existingInvitation = invitationRepository.findById(id).get();
			String toEmail = null;
			if(emailRepository.findById(existingInvitation.getEmail().getId()).isPresent()) {
				if(InvitationType.RejoindrePlateform.equals(existingInvitation.getType())){
					toEmail = employeRepository.findById(existingInvitation.getEmail().getIdRecepteur()).get().getEmail();
					emailService.sendEmail(toEmail,"Rappel : "+existingInvitation.getEmail().getDescription(),existingInvitation.getEmail().getTitre(),null);
				}
				else if(InvitationType.Voyage.equals(existingInvitation.getType())){
					toEmail = utilisateurRepository.findById(existingInvitation.getEmail().getIdRecepteur()).get().getEmail();
					emailService.sendEmail(toEmail,"Rappel : "+existingInvitation.getEmail().getDescription(),existingInvitation.getEmail().getTitre(),fileService.getFile_path());
				}
				
				emailRepository.save(existingInvitation.getEmail());
			}
		}
		else {
	        throw new ApiRequestException("Invitation non existe");
	    }		
		return invitationResponse;
		
		
	}
	
	
	@Override
	public InvitationResponse cancelInvitation(int id) throws IllegalStateException, IOException, MessagingException {
		//add check to verifiy if invitation date is not the same as voyage date
				 //add check if status already accepted
		//add email notification thatt invitation is canceled
				InvitationResponse invitationResponse = new InvitationResponse();
				Utilisateur utilisateur = new Utilisateur();
					Invitation existingInvitation = validateInvitationById(id,ActionType.Cancel);
						existingInvitation.setStatut(false);
						existingInvitation.setFile(fileService.deletFile(existingInvitation.getFile()));
						utilisateur = utilisateurRepository.findById(ConnectedUser.id).get();
						Voyage existingVoyage = existingInvitation.getInvitation_voyage();
						List<Voyage> voyages = utilisateur.getVoyages();
						voyages.remove(existingVoyage);
						utilisateur.setVoyages(voyages);
						emailService.sendEmail(existingInvitation.getId_envoyeur().getEmail(), "L'invitation avec id : "+existingInvitation.getId()+" a été annulé par  "+existingInvitation.getId_recepteur().getNom(), "invitation annulé", null);
						invitationRepository.save(existingInvitation);
						voyageRepository.save(existingVoyage);
						utilisateurRepository.save(utilisateur);
						invitationResponse.setReport("invitation annuler avec succès");
					return invitationResponse;
				
	}
	
	
	@Override
	public InvitationResponse removeAcceptanceFile(int id) throws IllegalStateException, IOException {
		InvitationResponse invitationResponse = new InvitationResponse();
		if (invitationRepository.findById(id).isPresent()) {
			Invitation invitation = invitationRepository.findById(id).get();
			invitation.setFile(fileService.deletFile(invitation.getFile()));
			invitationRepository.save(invitation);
		}
		invitationResponse.setReport("fichier supprimé avec success");
		return invitationResponse;
	}

	@Override
	public InvitationResponse addAcceptanceFile(int id,MultipartFile file) throws IllegalStateException, IOException {
		InvitationResponse invitationResponse = new InvitationResponse();
		if (invitationRepository.findById(id).isPresent()) {
			Invitation invitation = invitationRepository.findById(id).get();
			invitation.setFile(fileService.uploadFile(file));
			invitationRepository.save(invitation);
		}
		invitationResponse.setReport("fichier ajouté avec success");
		return invitationResponse;
	}

	
	
	private void validateInvitationAgenda(Invitation existingInvitation) {
		if (existingInvitation.getEmploye().getId() == ConnectedUser.related_employe_id) {
            if (!voyageRepository.findById(existingInvitation.getInvitation_voyage().getId()).isPresent()) {
            	throw new ApiRequestException("Voyage liée a l'invitation non existe");
            }
        } else {
            throw new ApiRequestException("vous n'etez pas autoriser de faire cette action");
        }
		
	}
	
   
   private void validateInvitationModel(InvitationModel invitationModel) {
	    Employe existingEmploye = new Employe();
	    if (!employeRepository.findById(invitationModel.getEmploye().getId()).isPresent()) {
	        throw new ApiRequestException("employe non existe");
	    }
	    else {
	    	existingEmploye = employeRepository.findById(invitationModel.getEmploye().getId()).get();
	    if (existingEmploye.getDepartements().getEntreprise().getId() == ConnectedUser.enterpriseId) {
	        if (InvitationType.RejoindrePlateform.equals(InvitationType.valueOf(invitationModel.getType()))) {
	            if (utilisateurRepository.UserByCin(existingEmploye.getCin()) != null) {
	                throw new ApiRequestException("employe deja inscrit sur l'application");
	            } else if (InvitationType.Voyage.equals(InvitationType.valueOf(invitationModel.getType()))) {
	                Utilisateur utilisateur = utilisateurRepository.UserByCin(existingEmploye.getCin());
	                if (utilisateur == null) {
	                    throw new ApiRequestException("employe n'est pas inscrit sur l'application");
	                }
	                int voyageId = invitationModel.getVoyageModel().getId();
	                Voyage voyage = voyageRepository.findById(voyageId).get();
	                if (utilisateur.getVoyages().contains(voyage)) {
	                    throw new ApiRequestException("vous aves deja accepter une invitation pour cette voyage");
	                }
	            }
	        }
	    } else {
	        throw new ApiRequestException("vous n'etez pas autoriser d'envoyer des invitations a ce employe ");
	    }
	    }

	}
   
   
   private Invitation validateInvitationById(int id, ActionType actionType) {

	   if (invitationRepository.findById(id).isPresent()) {
	     Invitation existingInvitation = invitationRepository.findById(id).get();
	     if (ActionType.Modification.equals(actionType) || ActionType.Suppression.equals(actionType)) {
	       if (existingInvitation.getId_envoyeur().getId() != ConnectedUser.id &&
	         !(existingInvitation.getEnterprise().getId() == ConnectedUser.enterpriseId && ConnectedUser.type.equals(UserType.Personne_morale))) {
	         throw new ApiRequestException("vous n'etez pas autoriser de faire cette action");
	       } else {
	         return existingInvitation;
	       }
	     } else if (ActionType.Consultation.equals(actionType)) {
	         if (!(UserType.Personne_morale.equals(ConnectedUser.type) && existingInvitation.getEnterprise().getId() == ConnectedUser.enterpriseId) &&
	           !(UserType.Personne_physique.equals(ConnectedUser.type) && existingInvitation.getId_recepteur().getId() == ConnectedUser.id)) {
	           throw new ApiRequestException("vous n'etez pas autoriser de consulter cette invitation");
	         } else {
	           return existingInvitation;
	         }
	       }
	     else if (ActionType.Accept.equals(actionType)) {
	    	 if(InvitationType.RejoindrePlateform.equals(existingInvitation.getType()) && !(existingInvitation.getId_envoyeur().getId() == ConnectedUser.id)) {
	    		 throw new ApiRequestException("vous n'etez pas autoriser d'accepter cette invitation pour rejoindre le plateforme"); 
	    	 }
	    	 else if(InvitationType.Voyage.equals(existingInvitation.getType()) && !(existingInvitation.getId_recepteur().getId() == ConnectedUser.id)) {
	    		 throw new ApiRequestException("vous n'etez pas autoriser d'accepter cette invitation de voyage"); 
	    	 }
	    	 else {
	    		 return existingInvitation;
	    	 }
	     }
	     else if(ActionType.Cancel.equals(actionType)){
	    	 if(!(InvitationType.Voyage.equals(existingInvitation.getType()) && existingInvitation.getId_recepteur().getId() == ConnectedUser.id)) {
	    		 throw new ApiRequestException("vous n'etez pas autoriser d'annuler cette invitation"); 
	    		 
	    	 }
	    	 else {
	    		 return existingInvitation;
	    	 }
	  
	     }
	   } else {
	     throw new ApiRequestException("Invitation non existe");
	   }
	   return new Invitation();
	 }
   
   
   private InvitationModel buildInvitationModelFromInvitation(Invitation invitation) {
	    InvitationModel invitaiontModel = new InvitationModel();
		invitaiontModel.setId(invitation.getId());
		invitaiontModel.setDescription(invitation.getDescription());
		invitaiontModel.setTitre(invitation.getTitre());
		invitaiontModel.setType(invitation.getType().toString());
		invitaiontModel.setFichier_acceptation(invitation.getFile());
		if(invitation.getEmail() != null) {
		EmailModel emailModel = new EmailModel();
		emailModel.setId(invitation.getEmail().getId());
		emailModel.setTitre(invitation.getEmail().getTitre());
		emailModel.setDescription(invitation.getEmail().getDescription());
		emailModel.setEnvoyer_par(utilisateurRepository.findById(invitation.getEmail().getIdEnvoyeur()).get().getNom());
		invitaiontModel.setEmailModel(emailModel);
	    }
		
		if(invitation.getEmploye() != null) {
			EmployeModel employeModel = new EmployeModel();
			employeModel.setNom(invitation.getEmploye().getNom());
			invitaiontModel.setEmploye(employeModel);
		}
		
		if(invitation.getEnterprise() != null) {
			EnterpriseModel enterpriseModel = new EnterpriseModel();
			enterpriseModel.setNom(invitation.getEnterprise().getNom());
			invitaiontModel.setEnterprise(enterpriseModel);
		}
		
		if(invitation.getInvitation_voyage() != null) {
		VoyageModel voyageModel = new VoyageModel();
		voyageModel.setId(invitation.getInvitation_voyage().getId());
		voyageModel.setNom(invitation.getInvitation_voyage().getNom());
		voyageModel.setDateDebut(invitation.getInvitation_voyage().getDateDebut());
		voyageModel.setDateFin(invitation.getInvitation_voyage().getDateFin());
		if (invitation.isStatut()) {
            voyageModel.setStatut_invitation("invitation accepter");
        } else {
            voyageModel.setStatut_invitation("invitation non accepter");
        }
		invitaiontModel.setVoyageModel(voyageModel);
		}
		
		invitaiontModel.setStatut(invitation.isStatut());
	   
	   
	   return invitaiontModel;
	   
	   
   }


   
  

	
	
	
}
