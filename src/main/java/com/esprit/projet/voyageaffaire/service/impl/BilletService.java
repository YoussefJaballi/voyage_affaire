package com.esprit.projet.voyageaffaire.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.esprit.projet.voyageaffaire.apimodel.BilletModel;
import com.esprit.projet.voyageaffaire.entity.Billet;
import com.esprit.projet.voyageaffaire.entity.Departement;
import com.esprit.projet.voyageaffaire.entity.Employe;
import com.esprit.projet.voyageaffaire.entity.Vol;
import com.esprit.projet.voyageaffaire.repository.BilletRepository;
import com.esprit.projet.voyageaffaire.repository.HotelRepository;
import com.esprit.projet.voyageaffaire.repository.VolRepository;

@Service
public class BilletService {

	@Autowired
	BilletRepository billetRepository;
	@Autowired
	VolRepository volRepository;
	@Autowired
	EmailService emailService;

	public List<Billet> getAllBillets() {
		System.out.println("Get All Billets...");
		List<Billet> Billets = new ArrayList<>();
		billetRepository.findAll().forEach(Billets::add);
		return Billets;

	}

	public Billet addBillet(Billet billet) {
		return billetRepository.save(billet);
	}

	public Billet updateBillet(int id, Billet newBillet) {
		if (billetRepository.findById(id).isPresent()) {
			Billet existingBillet = billetRepository.findById(id).get();
			existingBillet.setPrix(newBillet.getPrix());
			existingBillet.setDate(newBillet.getDate());
			existingBillet.setHeure(newBillet.getHeure());
			existingBillet.setNom_Passport(newBillet.getNom_Passport());
			return billetRepository.save(existingBillet);
		} else
			return null;
	}

	public String deleteBillet(int id) {
		if (billetRepository.findById(id).isPresent()) {
			billetRepository.deleteById(id);
			return "Billet supprimé";
		} else
			return "Billet non supprimé";
	}

	public void affecterBilletAVol(int billetId, int volId) {
		// TODO Auto-generated method stub
		Vol volEntity = volRepository.findById(volId).get();
		Billet billetEntity = billetRepository.findById(billetId).get();

		if (volEntity.getBillets() == null) {

			List<Billet> billets = new ArrayList<>();
			billets.add(billetEntity);
			volEntity.setBillets(billets);
		} else {

			volEntity.getBillets().add(billetEntity);
		}

		// à ajouter?
		volRepository.save(volEntity);
	}

	public String sendBillet(BilletModel billetModel) throws MessagingException {
		Billet billet = billetRepository.findById(billetModel.getId()).get();
		if (billet != null) {
			emailService.sendEmail(billetModel.getEmail(), String.valueOf(billet.getId()), "BilletVoyage", null);
			return "email  envoyer avec success";
		}
		return "donneé manquant";
	}

}
