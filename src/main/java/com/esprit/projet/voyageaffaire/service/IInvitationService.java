package com.esprit.projet.voyageaffaire.service;

import java.io.IOException;

import javax.mail.MessagingException;

import org.springframework.web.multipart.MultipartFile;

import com.esprit.projet.voyageaffaire.apimodel.InvitationModel;
import com.esprit.projet.voyageaffaire.apimodel.InvitationResponse;

public interface IInvitationService {

	public InvitationResponse updateInvitation(int id, InvitationModel newInvitation);
	public InvitationResponse deleteInvitation(int id);
	public InvitationResponse getInvitations();
	public InvitationResponse getInvitationById(int id);
	public InvitationResponse addInvitation(InvitationModel invitationModel) throws MessagingException;
	public InvitationResponse acceptInvitation(int id,MultipartFile file) throws IllegalStateException, IOException;
	public InvitationResponse cancelInvitation(int id) throws IllegalStateException, IOException, MessagingException;
	public InvitationResponse addtoagenda(int id, int idagenda);
	public InvitationResponse resendmail(int id) throws MessagingException;
	public InvitationResponse removeAcceptanceFile(int id) throws IllegalStateException, IOException;
	public InvitationResponse addAcceptanceFile(int id,MultipartFile file) throws IllegalStateException, IOException;
}
