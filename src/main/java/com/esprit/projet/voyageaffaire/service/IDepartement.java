package com.esprit.projet.voyageaffaire.service;

import java.util.List;

import com.esprit.projet.voyageaffaire.entity.Departement;


public interface IDepartement {
	public List<Departement> getAllDepartements();
}
