package com.esprit.projet.voyageaffaire.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.esprit.projet.voyageaffaire.apimodel.AgendaModel;
import com.esprit.projet.voyageaffaire.apimodel.AgendaResponse;
import com.esprit.projet.voyageaffaire.apimodel.EmailModel;
import com.esprit.projet.voyageaffaire.apimodel.InvitationModel;
import com.esprit.projet.voyageaffaire.apimodel.InvitationResponse;
import com.esprit.projet.voyageaffaire.apimodel.VoyageModel;
import com.esprit.projet.voyageaffaire.entity.Agenda;
import com.esprit.projet.voyageaffaire.entity.Invitation;
import com.esprit.projet.voyageaffaire.entity.Voyage;
import com.esprit.projet.voyageaffaire.enums.UserType;
import com.esprit.projet.voyageaffaire.repository.AgendaRepository;
import com.esprit.projet.voyageaffaire.repository.UtilisateurRepository;
import com.esprit.projet.voyageaffaire.security.jwt.ConnectedUser;
import com.esprit.projet.voyageaffaire.service.IAgendaService;

@Service
public class AgendaService implements IAgendaService{

	@Autowired
	private AgendaRepository agendaRepository;
	
	
	@Autowired
	private UtilisateurRepository utilisateurRepository;

	
	@Override
	public AgendaResponse addAgenda(AgendaModel agendaModel) {
		//to be completed , check for dates
		AgendaResponse AgendaResponse = new AgendaResponse();
		Agenda agenda = new Agenda();
		agenda.setNom(agendaModel.getNom());
		agenda.setDateDebut(agendaModel.getDateDebut());
		agenda.setDateFin(agendaModel.getDateFin());
		agenda.setAgenda_utilisateur(utilisateurRepository.findById(ConnectedUser.id).get());
		agendaRepository.save(agenda);
		AgendaResponse.setReport("agenda ajouté avec succès");
		return AgendaResponse;
		
	}
	
	@Override
	public Agenda updateAgenda(int id, Agenda newAgenda) {
		if (agendaRepository.findById(id).isPresent()) {
			Agenda existingAgenda = agendaRepository.findById(id).get();
			existingAgenda.setNom(newAgenda.getNom());
			return agendaRepository.save(existingAgenda);
		} else
			return null;
	}
	
	
	@Override
	public String deleteAgenda(int id) {
		if (agendaRepository.findById(id).isPresent()) {
			agendaRepository.deleteById(id);
			return "Agenda supprimé";
		} else
			return "Agenda non supprimé";
	}
	
	
	@Override
	public AgendaResponse getAgendas() {
	    List < Agenda > agendas = agendaRepository.findAll();
	    List < AgendaModel > AgendaRespones = new ArrayList < AgendaModel > ();
	    AgendaResponse AgendaResponse = new AgendaResponse();

	    if (agendas != null && !agendas.isEmpty()) {
	        for (Agenda agenda: agendas) {
	            if (agenda.getAgenda_utilisateur().getId() == ConnectedUser.id || (agenda.getAgenda_utilisateur().getUtilisateur_entreprise().getId() == ConnectedUser.enterpriseId && ConnectedUser.type.equals(UserType.Personne_morale))) {
	                List < VoyageModel > voyages = new ArrayList < VoyageModel > ();
	                AgendaModel agendaModel = new AgendaModel();
	                agendaModel.setId(agenda.getId());
	                agendaModel.setNom(agenda.getNom());
	                agendaModel.setDateDebut(agenda.getDateDebut());
	                agendaModel.setDateFin(agenda.getDateFin());
	                agendaModel.setCreer_par(agenda.getAgenda_utilisateur().getNom());
	                for (Voyage voyage: agenda.getVoyages()) {
	                    VoyageModel voyageModel = new VoyageModel();
	                    voyageModel.setId(voyage.getId());
	                    voyageModel.setNom(voyage.getNom());
	                    voyageModel.setDateDebut(voyage.getDateDebut());
	                    voyageModel.setDateFin(voyage.getDateDebut());
	                    if(voyage.getInvitations() != null) {
	                    	for(Invitation invitation :  voyage.getInvitations()) {
	                    if (invitation.isStatut()) {
	                        voyageModel.setStatut_invitation("invitation accepter");
	                    } else {
	                        voyageModel.setStatut_invitation("invitation non accepter");
	                    }
	                    	}
	                }
	                    voyages.add(voyageModel);
	                }
	                agendaModel.setVoyages(voyages);
	                AgendaRespones.add(agendaModel);
	            }
	        }

	    }

	    AgendaResponse.setData(AgendaRespones);

	    return AgendaResponse;

	}
	
	
}
