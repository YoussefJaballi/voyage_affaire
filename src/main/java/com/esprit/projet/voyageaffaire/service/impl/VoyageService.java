package com.esprit.projet.voyageaffaire.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.esprit.projet.voyageaffaire.entity.Voyage;
import com.esprit.projet.voyageaffaire.repository.VoyageRepository;

@Service
public class VoyageService {

	@Autowired
	private VoyageRepository voyageRepository;

	public Voyage addVoyage(Voyage voyage) {
		return voyageRepository.save(voyage);
	}
	public Voyage updateVoyage(int id, Voyage newVoyage) {
		if (voyageRepository.findById(id).isPresent()) {
			Voyage existingVoyage = voyageRepository.findById(id).get();
			existingVoyage.setNom(newVoyage.getNom());
			return voyageRepository.save(existingVoyage);
		} else
			return null;
	}
	public String deleteVoyage(int id) {
		if (voyageRepository.findById(id).isPresent()) {
			voyageRepository.deleteById(id);
			return "voyage supprimé";
		} else
			return "voyage non supprimé";
	}
	
	public List<String> getAllVoyageByVol(int adrid) {
		return voyageRepository.getAllVoyageByVol(adrid);
	}
}
