package com.esprit.projet.voyageaffaire.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.esprit.projet.voyageaffaire.entity.Chambre;
import com.esprit.projet.voyageaffaire.entity.Facture;
import com.esprit.projet.voyageaffaire.entity.Hotel;
import com.esprit.projet.voyageaffaire.entity.Invitation;
import com.esprit.projet.voyageaffaire.entity.Voyage;
import com.esprit.projet.voyageaffaire.entity.Employe;

import com.esprit.projet.voyageaffaire.repository.ChambreRepository;
import com.esprit.projet.voyageaffaire.repository.DepartementRepository;
import com.esprit.projet.voyageaffaire.repository.EmployeRepository;
import com.esprit.projet.voyageaffaire.repository.HotelRepository;
import com.esprit.projet.voyageaffaire.repository.InvitationRepository;
import com.esprit.projet.voyageaffaire.repository.VoyageRepository;






@Service
public class ChambreService {

	@Autowired
	private ChambreRepository chambreRepository;
	@Autowired
	EmployeRepository employeRepository;
	@Autowired
	HotelRepository hotelRepository;
	@Autowired
	VoyageRepository voyageRepository;
	@Autowired
	InvitationRepository invitationRepository;
	
	
	public List<Chambre>getAllChambres(){
		System.out.println("Get All Chambres...");
		List<Chambre> chambres=new ArrayList<>();
		chambreRepository.findAll().forEach(chambres::add);
		return chambres;
		
	}
	
	public Chambre addChambre(Chambre chambre) {
		return chambreRepository.save(chambre);
	}
	
	public Chambre updateChambre(int id, Chambre newChambre) {
		if (chambreRepository.findById(id).isPresent()) {
			Chambre existingChambre = chambreRepository.findById(id).get();
			existingChambre.setNumero(newChambre.getNumero());
			existingChambre.setEtage(newChambre.getEtage());
			return chambreRepository.save(existingChambre);
		} else
			return null;
	}
	public String deleteChambre(int id) {
		if (chambreRepository.findById(id).isPresent()) {
			chambreRepository.deleteById(id);
			return "Chambre supprimé";
		} else
			return "Chambre non supprimé";
	}
	


	public Chambre getChambreById(int chambreId) {
		return chambreRepository.findById(chambreId).get();	
	}
	
}
