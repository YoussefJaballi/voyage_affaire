package com.esprit.projet.voyageaffaire.service.impl;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletContext;

import org.apache.tomcat.jni.Directory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.esprit.projet.voyageaffaire.security.jwt.ConnectedUser;
import com.esprit.projet.voyageaffaire.service.IFileService;
import javax.servlet.ServletContext;
import org.apache.commons.io.FileUtils;

@Service
public class FileService implements IFileService{
	
	@Value("${file_path}")
	private String file_path;
	
	
	@Value("${upload_directory}")
	private String upload_directory;

	@Override
	public String uploadFile(MultipartFile file) throws IllegalStateException, IOException
    {
		File dir = new File(upload_directory+ConnectedUser.nom);
		if(dir.mkdir()) {
        file.transferTo(new File(dir+"/"+file.getOriginalFilename()));
        return "http://localhost:8080/applicationDirectory/uploaddirectory/"+ConnectedUser.nom+"/"+file.getOriginalFilename();
		}
		else {
			return null;
		}
    }
	
	@Override
	public String deletFile(String filepath) throws IllegalStateException, IOException
    {
		
		String newFilePath = filepath.replace("http://localhost:8080/applicationDirectory/uploaddirectory/", upload_directory);
		File file  = new File(newFilePath);
		if(file.delete()) {
			FileUtils.deleteDirectory(file.getParentFile());
			return null;
		}
		else {
			return filepath;
		}
    }

	@Override
	 public MediaType getMediaTypeForFileName(ServletContext servletContext, String fileName) {
	        // application/pdf
	        // application/xml
	        // image/gif, ...
	        String mineType = servletContext.getMimeType(fileName);
	        try {
	            MediaType mediaType = MediaType.parseMediaType(mineType);
	            return mediaType;
	        } catch (Exception e) {
	            return MediaType.APPLICATION_OCTET_STREAM;
	        }
	    }
	
	public String getFile_path() {
		return file_path;
	}

	public void setFile_path(String file_path) {
		this.file_path = file_path;
	}

	public String getUpload_directory() {
		return upload_directory;
	}

	public void setUpload_directory(String upload_directory) {
		this.upload_directory = upload_directory;
	}
	
	
}
