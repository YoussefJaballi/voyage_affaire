package com.esprit.projet.voyageaffaire.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.esprit.projet.voyageaffaire.entity.Departement;
import com.esprit.projet.voyageaffaire.repository.DepartementRepository;

@Service
public class DepartementService {
	@Autowired
	DepartementRepository deptRepoistory;
	
	
	public Departement getDepartementById(int DepartementId) {
		return deptRepoistory.findById(DepartementId).get();	
	}
}
