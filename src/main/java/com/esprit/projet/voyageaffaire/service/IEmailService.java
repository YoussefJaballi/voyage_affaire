package com.esprit.projet.voyageaffaire.service;

import javax.mail.MessagingException;

import com.esprit.projet.voyageaffaire.entity.Email;

public interface IEmailService {

	
	public Email addEmail(Email email) throws MessagingException;
	public Email updateEmail(int id, Email newEmail);
	public String deleteEmail(int id);
	public void sendEmail(String toEmail, String body, String subject, String attachment) throws MessagingException;
	
}
