package com.esprit.projet.voyageaffaire.service;

import com.esprit.projet.voyageaffaire.apimodel.AgendaModel;
import com.esprit.projet.voyageaffaire.apimodel.AgendaResponse;
import com.esprit.projet.voyageaffaire.entity.Agenda;

public interface IAgendaService {
	public AgendaResponse addAgenda(AgendaModel agendaModel);
	public Agenda updateAgenda(int id, Agenda newAgenda);
	public String deleteAgenda(int id);
	public AgendaResponse getAgendas();
}
