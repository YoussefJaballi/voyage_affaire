package com.esprit.projet.voyageaffaire.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.esprit.projet.voyageaffaire.entity.Adresse;
import com.esprit.projet.voyageaffaire.entity.Facture;
import com.esprit.projet.voyageaffaire.entity.Hotel;
import com.esprit.projet.voyageaffaire.repository.AdresseRepository;


@Service
public class AdresseService {
	
	@Autowired
	AdresseRepository adresseRepository;
	
	public Adresse getAdresseById(int adresseId) {
		return adresseRepository.findById(adresseId).get();	
	}
	public List<Adresse>getAllAdresses(){
		System.out.println("Get All Adresses...");
		List<Adresse> adresses=new ArrayList<>();
		adresseRepository.findAll().forEach(adresses::add);
		return adresses;
		
	}
	
	public Adresse addAdresse(Adresse adresse) {
		return adresseRepository.save(adresse);
	}
	
	public Adresse updateAdresse(int id, Adresse newAdresse) {
		if (adresseRepository.findById(id).isPresent()) {
			Adresse existingAdresse = adresseRepository.findById(id).get();
			existingAdresse.setNom(newAdresse.getNom());
			
			return adresseRepository.save(existingAdresse);
		} else
			return null;
	}
	public String deleteAdresse(int id) {
		if (adresseRepository.findById(id).isPresent()) {
			adresseRepository.deleteById(id);
			return "Adresse supprimé";
		} else
			return "Adresse non supprimé";
	}
	
	

}
