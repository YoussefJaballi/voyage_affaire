package com.esprit.projet.voyageaffaire.service;

import java.io.IOException;

import javax.servlet.ServletContext;

import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;

public interface IFileService {
	
	public String uploadFile(MultipartFile file) throws IllegalStateException, IOException;
	public String deletFile(String filepath) throws IllegalStateException, IOException;
	public MediaType getMediaTypeForFileName(ServletContext servletContext, String fileName);

}
