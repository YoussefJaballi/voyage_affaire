package com.esprit.projet.voyageaffaire.service;

import java.util.Date;
import java.util.List;

import com.esprit.projet.voyageaffaire.entity.Contrat;
import com.esprit.projet.voyageaffaire.entity.Employe;
import com.esprit.projet.voyageaffaire.entity.Entreprise;



public interface IEmployeService {
	public Employe addEmploye(Employe employe);
	public Employe updateEmploye(int id, Employe newEmploye);
	public String deleteEmploye(int id);
	public void mettreAjourEmailByEmployeId(String email, int employeId);
	public void affecterEmployeADepartement(int employeId, int depId);
	public void desaffecterEmployeDuDepartement(int employeId, int depId);
	public int ajouterContrat(Contrat contrat);
	public void affecterContratAEmploye(int contratId, int employeId);
	public String getEmployeNomById(int employeId);	
	public void deleteContratById(int contratId);
	public int getNombreEmployeJPQL();
	public List<String> getAllEmployeNamesJPQL();
	public List<Employe> getAllEmployeByEntreprise(Entreprise entreprise);
	public void mettreAjourEmailByEmployeIdJPQL(String email, int employeId);
	public void deleteAllContratJPQL();
	public float getSalaireByEmployeIdJPQL(int employeId);
	public Double getSalaireMoyenByDepartementId(int departementId);
	public List<Employe> getAllEmployes();


}
