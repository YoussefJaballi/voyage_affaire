package com.esprit.projet.voyageaffaire.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.esprit.projet.voyageaffaire.entity.Adresse;
import com.esprit.projet.voyageaffaire.entity.Chambre;
import com.esprit.projet.voyageaffaire.entity.Contrat;
import com.esprit.projet.voyageaffaire.entity.Departement;
import com.esprit.projet.voyageaffaire.entity.Employe;
import com.esprit.projet.voyageaffaire.entity.Entreprise;
import com.esprit.projet.voyageaffaire.entity.Hotel;
import com.esprit.projet.voyageaffaire.repository.ChambreRepository;
import com.esprit.projet.voyageaffaire.repository.ContratRepository;
import com.esprit.projet.voyageaffaire.repository.DepartementRepository;
import com.esprit.projet.voyageaffaire.repository.EmployeRepository;
import com.esprit.projet.voyageaffaire.service.IEmployeService;




@Service
public class EmployeService implements IEmployeService {

	@Autowired
	private EmployeRepository employeRepository;
	@Autowired
	DepartementRepository deptRepoistory;
	@Autowired
	ContratRepository contratRepoistory;
	@Autowired
	ChambreRepository chambreRepoistory;
	
	public List<Employe>getAllEmployess(){
		System.out.println("Get All Employes...");
		List<Employe> employes=new ArrayList<>();
		employeRepository.findAll().forEach(employes::add);
		return employes;
		
	}
	

	public Employe addEmploye(Employe employe) {
		return employeRepository.save(employe);
	}
	
	
	public Employe updateEmploye(int id, Employe newEmploye) {
		if (employeRepository.findById(id).isPresent()) {
			Employe existingEmploye = employeRepository.findById(id).get();
			existingEmploye.setNom(newEmploye.getNom());
			return employeRepository.save(existingEmploye);
		} else
			return null;
	}
	public String deleteEmploye(int id) {
		if (employeRepository.findById(id).isPresent()) {
			employeRepository.deleteById(id);
			return "Employe supprimé";
		} else
			return "Employe non supprimé";
	}
	@Override
	public void mettreAjourEmailByEmployeId(String email, int employeId) {
		// TODO Auto-generated method stub
		Employe employe = employeRepository.findById(employeId).get();
		employe.setEmail(email);
		employeRepository.save(employe);
		
	}
	@Override
	public void affecterEmployeADepartement(int employeId, int depId) {
		// TODO Auto-generated method stub
		Departement depEntity = deptRepoistory.findById(depId).get();
		Employe employeEntity = employeRepository.findById(employeId).get();

		if(depEntity.getEmployes() == null){

			List<Employe> employes = new ArrayList<>();
			employes.add(employeEntity);
			depEntity.setEmployes(employes);
		}else{

			depEntity.getEmployes().add(employeEntity);
		}

		// à ajouter? 
		deptRepoistory.save(depEntity); 
	}
		
	
	@Override
	public void desaffecterEmployeDuDepartement(int employeId, int depId) {
		// TODO Auto-generated method stub
		Departement dep = deptRepoistory.findById(depId).get();

		int employeNb = dep.getEmployes().size();
		for(int index = 0; index < employeNb; index++){
			if(dep.getEmployes().get(index).getId() == employeId){
				dep.getEmployes().remove(index);
				deptRepoistory.save(dep); 

				break;
			}
		}

	}
	@Override
	public int ajouterContrat(Contrat contrat) {
		// TODO Auto-generated method stub
		contratRepoistory.save(contrat);
		return contrat.getReference();
	}
	@Override
	public void affecterContratAEmploye(int contratId, int employeId) {
		// TODO Auto-generated method stub
		Contrat contratEntity = contratRepoistory.findById(contratId).get();
		Employe employeEntity = employeRepository.findById(employeId).get();

		contratEntity.setEmploye(employeEntity);
		contratRepoistory.save(contratEntity);
	}
	@Override
	public String getEmployeNomById(int employeId) {
		// TODO Auto-generated method stub
		Employe employeManagedEntity = employeRepository.findById(employeId).get();
		return employeManagedEntity.getNom();
	}
	
	@Override
	public void deleteContratById(int contratId) {
		// TODO Auto-generated method stub
		Contrat contratManagedEntity = contratRepoistory.findById(contratId).get();
		contratRepoistory.delete(contratManagedEntity);
		
	}
	@Override
	public int getNombreEmployeJPQL() {
		// TODO Auto-generated method stub
		return employeRepository.countemp();
	}
	
	public int getNombreEmployeDepartement(Departement departementid) {
		// TODO Auto-generated method stub
		return employeRepository.countEmpByDepartement(departementid);
	}
	
	@Override
	public List<String> getAllEmployeNamesJPQL() {
		// TODO Auto-generated method stub
		return employeRepository.employeNames();
	}
	@Override
	public List<Employe> getAllEmployeByEntreprise(Entreprise entreprise) {
		// TODO Auto-generated method stub
		return employeRepository.getAllEmployeByEntreprisec(entreprise);
	}
	@Override
	public void mettreAjourEmailByEmployeIdJPQL(String email, int employeId) {
		employeRepository.mettreAjourEmailByEmployeIdJPQL(email, employeId);

		
	}
	@Override
	public void deleteAllContratJPQL() {
		// TODO Auto-generated method stub
		employeRepository.deleteAllContratJPQL();


	}
	@Override
	public float getSalaireByEmployeIdJPQL(int employeId) {
		// TODO Auto-generated method stub
		return (Float) null;
	}
	@Override
	public Double getSalaireMoyenByDepartementId(int departementId) {
		return employeRepository.getSalaireMoyenByDepartementId(departementId);
		
	}
	@Override
	public List<Employe> getAllEmployes() {
		// TODO Auto-generated method stub
		return (List<Employe>) employeRepository.findAll();
	}
	
//	public Employe getAllEmployeByDepartement(Departement departement) {
//		return employeRepository.getAllEmployeByDepartement(departement);
//	}
	public List<Employe> getAllEmployeByDepartement(Departement departement) {
		return (List<Employe>) employeRepository.getAllEmployeByDepartement(departement);
	}
	public List<String> getAllEmployeByAdresse(int adrid) {
		return employeRepository.getAllEmployeByAdresse(adrid);
	}
	public List<Employe> getAllEmployeByAdress(Adresse adresse){
		return employeRepository.getAllEmployeByAdress(adresse);
	}
	
	public List<Employe> getAllEmployeByHotel(Hotel hotel) {
		return employeRepository.getAllEmployeByHotel(hotel);
	}

//affecter
	public void affecterEmployeAChambre(int employeId, int chaId,Departement departement ) {

		Chambre chEntity = chambreRepoistory.findById(chaId).get();
		Employe employeEntity = employeRepository.findById(employeId).get();

		if(employeEntity.getEmploye_chambre()== null ){

			List<Employe> employes = getAllEmployeByDepartement(departement);
			employes.add(employeEntity);
			employeEntity.setEmploye_chambre(chEntity);
		}else{

			employeEntity.setEmploye_chambre(chEntity);
			
		}

		   // chambreRepoistory.save(chEntity); 
		employeRepository.save(employeEntity);
	}
	
	
	
}
