package com.esprit.projet.voyageaffaire.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.esprit.projet.voyageaffaire.entity.Adresse;
import com.esprit.projet.voyageaffaire.entity.Chambre;
import com.esprit.projet.voyageaffaire.entity.Hotel;
import com.esprit.projet.voyageaffaire.entity.Voyage;
import com.esprit.projet.voyageaffaire.repository.HotelRepository;






@Service
public class HotelService {

	
	@Autowired
	HotelRepository hotelRepository;
	
	
	public List<Hotel>getAllHotels(){
		System.out.println("Get All Hotels...");
		List<Hotel> hotels=new ArrayList<>();
		hotelRepository.findAll().forEach(hotels::add);
		return hotels;
		
	}
	
	public Hotel addHotel (Hotel  hotel ) {
		return hotelRepository.save(hotel);
	}
	public Hotel updateHotel(int id, Hotel newHotel) {
		if (hotelRepository.findById(id).isPresent()) {
			Hotel existingHotel = hotelRepository.findById(id).get();
			existingHotel.setNom(newHotel.getNom());
			return hotelRepository.save(existingHotel);
		} else
			return null;
	}
	public String deleteHotel(int id) {
		if (hotelRepository.findById(id).isPresent()) {
			hotelRepository.deleteById(id);
			return "hotel supprimé";
		} else
			return "hotel non supprimé";
	}
	public List<String> getAllHotelByAdresse(int adrid) {
		return hotelRepository.getAllHotelByAdresse(adrid);
	}
	
	public Hotel getHotelById(int hotelId) {
		return hotelRepository.findById(hotelId).get();	
	}


	
}
