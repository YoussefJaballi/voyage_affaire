package com.esprit.projet.voyageaffaire.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.esprit.projet.voyageaffaire.entity.Contrat;
import com.esprit.projet.voyageaffaire.entity.Departement;
import com.esprit.projet.voyageaffaire.entity.Employe;
import com.esprit.projet.voyageaffaire.entity.Entreprise;
import com.esprit.projet.voyageaffaire.repository.DepartementRepository;
import com.esprit.projet.voyageaffaire.repository.EnterpriseRepository;
import com.esprit.projet.voyageaffaire.service.IEntrepriseService;



@Service
public class EntrepriseService implements IEntrepriseService{
	@Autowired
	EnterpriseRepository entrepriseRepoistory;
	@Autowired
	DepartementRepository deptRepoistory;
	@Override
	public int ajouterEntreprise(Entreprise entreprise) {
		entrepriseRepoistory.save(entreprise);
		return entreprise.getId();
	}

	@Override
	public int ajouterDepartement(Departement dep) {
		// TODO Auto-generated method stub
		deptRepoistory.save(dep);
		return dep.getId();
	}

	@Override
	public void affecterDepartementAEntreprise(int depId, int entrepriseId) {
		// TODO Auto-generated method stub
		Entreprise entrepriseManagedEntity = entrepriseRepoistory.findById(entrepriseId).get();
		Departement depManagedEntity = deptRepoistory.findById(depId).get();
		
		depManagedEntity.setEntreprise(entrepriseManagedEntity);
		deptRepoistory.save(depManagedEntity);
	}

	@Override
	public List<String> getAllDepartementsNamesByEntreprise(int entrepriseId) {
		// TODO Auto-generated method stub
		Entreprise entrepriseManagedEntity = entrepriseRepoistory.findById(entrepriseId).get();
		List<String> depNames = new ArrayList<>();
		for(Departement dep : entrepriseManagedEntity.getDepartements()){
			depNames.add(dep.getName());
		}
		
		return depNames;
	}

	@Override
	public void deleteEntrepriseById(int entrepriseId) {
		// TODO Auto-generated method stub
		entrepriseRepoistory.delete(entrepriseRepoistory.findById(entrepriseId).get());	

	}

	@Override
	public void deleteDepartementById(int depId) {
		// TODO Auto-generated method stub
		deptRepoistory.delete(deptRepoistory.findById(depId).get());	

	}

	@Override
	public Entreprise getEntrepriseById(int entrepriseId) {
		// TODO Auto-generated method stub
		return entrepriseRepoistory.findById(entrepriseId).get();	
	}

	

}
