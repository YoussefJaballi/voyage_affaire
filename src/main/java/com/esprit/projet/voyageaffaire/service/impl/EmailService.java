package com.esprit.projet.voyageaffaire.service.impl;

import java.io.File;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.Pageable;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.esprit.projet.voyageaffaire.entity.Email;
import com.esprit.projet.voyageaffaire.entity.Employe;
import com.esprit.projet.voyageaffaire.entity.Invitation;
import com.esprit.projet.voyageaffaire.entity.Utilisateur;
import com.esprit.projet.voyageaffaire.enums.InvitationType;
import com.esprit.projet.voyageaffaire.errors.ApiRequestException;
import com.esprit.projet.voyageaffaire.repository.EmailRepository;
import com.esprit.projet.voyageaffaire.repository.EmployeRepository;
import com.esprit.projet.voyageaffaire.repository.UtilisateurRepository;
import com.esprit.projet.voyageaffaire.service.IEmailService;

@Service
public class EmailService implements IEmailService{

	@Autowired
	private EmailRepository emailRepository;

	@Autowired
	EmployeRepository employeRepository;
	
	@Autowired
	UtilisateurRepository utilisateurRepository;
	
	@Autowired
    private JavaMailSender mailSender;
	
	@Override
	public Email addEmail(Email email) throws MessagingException {
		String toEmail = null;
		String file = null;
			if(InvitationType.RejoindrePlateform.equals(email.getInvitation().getType())){
				Employe employe = employeRepository.findById(email.getIdRecepteur()).get();
				toEmail = employe.getEmail();
			}
			else {
				Utilisateur utilisateur = utilisateurRepository.findById(email.getIdRecepteur()).get();
				toEmail = utilisateur.getEmail();
				file = "C:\\Users\\youss\\Desktop\\projet PI_2021\\Invitation_voyage.docx";
			}
			sendEmail(toEmail,email.getDescription(),email.getTitre(),file);
		return emailRepository.save(email);
	}
	
	@Override
	public Email updateEmail(int id, Email newEmail) {
		if (emailRepository.findById(id).isPresent()) {
			Email existingEmail = emailRepository.findById(id).get();
			existingEmail.setDescription(newEmail.getDescription());
			return emailRepository.save(existingEmail);
		} else
			return null;
	}
	
	@Override
	public String deleteEmail(int id) {
		if (emailRepository.findById(id).isPresent()) {
			emailRepository.deleteById(id);
			return "email supprimé";
		} else
			return "email non supprimé";
	}
	
	
	@Override
	public void sendEmail(String toEmail,
		    String body,
		    String subject,
		    String attachment) throws MessagingException {

		    MimeMessage mimeMessage = mailSender.createMimeMessage();

		    MimeMessageHelper mimeMessageHelper= new MimeMessageHelper(mimeMessage, true);

		    mimeMessageHelper.setFrom("spring.email.from@gmail.com");
		    mimeMessageHelper.setTo(toEmail);
		    mimeMessageHelper.setText(body);
		    mimeMessageHelper.setSubject(subject);

		    if(attachment != null) {
		    FileSystemResource fileSystem
		        = new FileSystemResource(new File(attachment));

		    mimeMessageHelper.addAttachment(fileSystem.getFilename(),
		        fileSystem);
		    }
		    mailSender.send(mimeMessage);
		    System.out.println("Mail Send...");

		}
	

	
	
}
