package com.esprit.projet.voyageaffaire.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

import com.esprit.projet.voyageaffaire.apimodel.FactureModel;
import com.esprit.projet.voyageaffaire.entity.Chambre;
import com.esprit.projet.voyageaffaire.entity.Facture;
import com.esprit.projet.voyageaffaire.entity.Voyage;
import com.esprit.projet.voyageaffaire.repository.FactureRepository;

@Service
public class FactureService {
	
	
	@Autowired
	FactureRepository factureRepository;
	@Autowired
	private EmailService emailService;

	public List<Facture>getAllFactures(){
		System.out.println("Get All Factures...");
		List<Facture> factures=new ArrayList<>();
		factureRepository.findAll().forEach(factures::add);
		return factures;
		
	}
	
	public Facture addFacture(Facture facture) {
		return factureRepository.save(facture);
	}
	
	public Facture updateFacture(int id, Facture newFacture) {
		if (factureRepository.findById(id).isPresent()) {
			Facture existingFacture = factureRepository.findById(id).get();
			existingFacture.setNumero(newFacture.getNumero());
			existingFacture.setDate(newFacture.getDate());
			return factureRepository.save(existingFacture);
		} else
			return null;
	}
	public String deleteFcture(int id) {
		if (factureRepository.findById(id).isPresent()) {
			factureRepository.deleteById(id);
			return "Facture supprimé";
		} else
			return "Facture non supprimé";
	}
	

	public List<Integer> getAllFactureByChambre(int chaid) {
		return factureRepository.getAllFactureByChambre(chaid);
	}
	
	public String sendFacture (FactureModel factureModel) throws MessagingException{
		Facture facture = factureRepository.findById(factureModel.getNum_facture()).get();
		if(facture !=null){
		emailService.sendEmail(factureModel.getEmail(),String.valueOf(facture.getNumero()), "FactureVhambre", null);
		return "email  envoyer avec success";
		}
		return "donneé manquant";
	}
	
	
}
