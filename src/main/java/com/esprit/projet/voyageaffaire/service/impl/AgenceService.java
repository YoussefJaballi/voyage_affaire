package com.esprit.projet.voyageaffaire.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.esprit.projet.voyageaffaire.entity.Agence;
import com.esprit.projet.voyageaffaire.entity.Employe;
import com.esprit.projet.voyageaffaire.repository.AgenceRepository;
@Service
public class AgenceService {
	private AgenceRepository agenceRepository;


	public Agence addAgence(Agence agence) {
		return agenceRepository.save(agence);
	}
	
	
	public List<Agence>getAllAgence(){
		System.out.println("Get All Agence...");
		List<Agence> agence=new ArrayList<>();
		agenceRepository.findAll().forEach(agence::add);
		return agence;
		
	}
	

	public Agence updateAgence(int id, Agence newAgence) {
		if (agenceRepository.findById(id).isPresent()) {
			Agence existingAgence = agenceRepository.findById(id).get();
			existingAgence.setName(newAgence.getName());
			return agenceRepository.save(existingAgence);
		} else
			return null;
	}
	public String deleteAgence(int id) {
		if (agenceRepository.findById(id).isPresent()) {
			agenceRepository.deleteById(id);
			return "Agence supprimé";
		} else
			return "Agence non supprimé";
	}
}
